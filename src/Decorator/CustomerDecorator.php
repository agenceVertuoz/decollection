<?php

namespace App\Decorator;

use App\Vertuoz\Decorator\AbstractDecorator;
use App\Vertuoz\Helper\UriHelper;
use App\Vertuoz\Helper\EncodingHelper;

class CustomerDecorator extends AbstractDecorator {

    const COMPONENT_CLASS = '\ModSpecific30Customer';

    public function __construct($component) {
        parent::__construct($component);
    }
    
    public function __toString() {
        return $this->getFullname();
    }
    
    public function getId() {
        return $this->Id;
    }

    public function getLastname() {
        return $this->AddressIdObject->Lastname;
    }

    public function getFirstname() {
        return $this->AddressIdObject->Firstname;
    }

    public function getCompany() {
        return $this->AddressIdObject->Company;
    }

    public function getAddress() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->Address;
        }
        return null;
    }

    public function getPostalCode() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->PostalCode;
        }
        return null;
    }

    public function getCity() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->City;
        }
        return null;
    }

    public function getState() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->State;
        }
        return null;
    }

    public function getEmail() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->AutomaticDefaultEmail;
        }
        return null;
    }

    public function getCountryId() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->CountryId;
        }
        return null;
    }

    public function getHomePhone() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->HomePhone;
        }
        return null;
    }

    public function getOfficePhone() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->OfficePhone;
        }
        return null;
    }

    public function getOfficeCellular() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->OfficeCellular;
        }
        return null;
    }

    public function getHomeCellular() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->HomeCellular;
        }
        return null;
    }

    public function getOfficeFax() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->OfficeFax;
        }
        return null;
    }

    public function getHomeFax() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->HomeFax;
        }
        return null;
    }
    
    public function getFullname() {
        
        $fullname = "";

        if (strlen($this->AddressIdObject->Lastname) > 0) {
            $fullname = $this->AddressIdObject->Lastname;
            if ($this->AddressIdObject->Firstname) {
                $fullname .= " " . $this->AddressIdObject->Firstname;
            }
        } elseif (strlen($this->AddressIdObject->Company) > 0) {
            $fullname = $this->AddressIdObject->Company;
        } else {
            $fullname = "No name / company";
        }
        
        return $fullname;
        
    }
    
    public function getPhone() {
        
        $telephone = [];
        
        if (!is_null($this->AddressId)) {
            if (!is_null($this->AddressIdObject->HomePhone) && strlen($this->AddressIdObject->HomePhone) > 0) {
                array_push($telephone, $this->AddressIdObject->HomePhone);
            }
            if (!is_null($this->AddressIdObject->HomeCellular) && strlen($this->AddressIdObject->HomeCellular) > 0) {
                array_push($telephone, $this->AddressIdObject->HomeCellular);
            }
            if (!is_null($this->AddressIdObject->OfficePhone) && strlen($this->AddressIdObject->OfficePhone) > 0) {
                array_push($telephone, $this->AddressIdObject->OfficePhone);
            }
            if (!is_null($this->AddressIdObject->OfficeCellular) && strlen($this->AddressIdObject->OfficeCellular) > 0) {
                array_push($telephone, $this->AddressIdObject->OfficeCellular);
            }
        }
        
        return implode('<br />', $telephone);
        
    }
    
    public function getCountry() {
        return (!is_null($this->AddressId) && !is_null($this->AddressIdObject->CountryId)) ? $this->AddressIdObject->CountryIdObject->Name : '-';
    }
    
}
