<?php

namespace App\Decorator;

use App\Vertuoz\Decorator\AbstractDecorator;
use App\Vertuoz\Helper\UriHelper;
use App\Vertuoz\Helper\EncodingHelper;

class PropertyDecorator extends AbstractDecorator {

    const COMPONENT_CLASS = '\ModSpecific30Property';
    const PHOTOS_PATH = 'http://rootcms.luxury-rentals.com/documents/30/specific30agents/property';
    const PHOTOS_SERVICES_PATH = 'http://rootcms.luxury-rentals.com/documents/30/specific30agents/services';
    const COMPLETE_DESCRIPTION_FILE = __DIR__ . "/../../../luxuryElocms/documents/30/specific30agents/complete_description";
    const COMPLETE_DESCRIPTION_PATH = 'http://rootcms.luxury-rentals.com/documents/30/specific30agents/complete_description';
    const PHOTOS_FULL_SIZE = "1920x1440";
    const MAIN_SLIDER_PHOTOS = 10;
    const OVERVIEW_PHOTOS = 2;

    public $uri;
    public $photos;
    public $services;
    public $amenities;
           
    protected $_join_property_service;
    protected $_service_category;
    protected $_join_property_amenity;
    //protected $_amenity_category;
    

//public function __construct($component, array $objJoinPropertyService = null, array $objServiceCategory = null, array $objJoinPropertyAmenity = null, array $objAmenityCategory = null) {
    public function __construct($component, array $objJoinPropertyService = null, array $objServiceCategory = null, array $objJoinPropertyAmenity = null, array $objAmenityCategory = null) {
        parent::__construct($component);
        
        $this->_join_property_service = $objJoinPropertyService;
        $this->_service_category = $objServiceCategory;
        $this->_join_property_amenity = $objJoinPropertyAmenity;
        //$this->_amenity_category = $objAmenityCategory;
        
        $this->photos = $this->getPhotos();
        $this->uri = $this->getUri();
        $this->services = $this->getServices();
        $this->amenities = $this->getAmenities();
        
    }
    
    public function getFullname() {
        return $this->Name." (".$this->Code.")";
    }
    
    public function getId() {
        return $this->Id;
    }
    
    public function getName() {
        return $this->Name;
    }
    
    public function getComment() {
        return $this->Comment;
    }
    
    public function getCurrency() {
        return (!is_null($this->CurrencyId)) ? " ".$this->CurrencyIdObject->Code."" : "";
    }
    
    public function getDeposit() {
        return (!is_null($this->Deposit)) ? $this->Deposit : 0;
    }
    
    public function getSaturdayToSaturday() {
        return $this->SaturdayToSaturday;
    }
    
    public function getProrataAllowed() {
        return $this->ProrataAllowed;
    }
    
    public function getMinWeeks() {
        return $this->MinWeeks;
    }
    
    public function getSingleBedrooms() {
        return $this->SingleBedrooms;
    }
    
    public function getDoubleBedrooms() {
        return $this->DoubleBedrooms;
    }
    
    public function getMaxSleeps() {
        return $this->MaxSleeps;
    }
    
    public function getShortDescription() {
        return html_entity_decode(EncodingHelper::fixUTF8($this->ShortDescription));
    }
    
    public function getDescription() {
        return html_entity_decode(EncodingHelper::fixUTF8($this->Description));
    }
    
    public function getCompleteFileDescription() {
        if (file_exists(self::COMPLETE_DESCRIPTION_FILE . '/' . $this->Code . '.pdf')) {
            return self::COMPLETE_DESCRIPTION_PATH . '/' . $this->Code . '.pdf';
        }
        return null;
    }

    public function getUri() {
        return '/' . UriHelper::slugify($this->Name) . '-' . $this->Id;
    }
    
    public function getRegion() {
        if (!is_null($this->RegionId)) {
            return $this->RegionIdObject->Name;
        }
        return null;
    }
    
    public function getCountry() {
        if (!is_null($this->RegionId) && !is_null($this->RegionIdObject->CountryId)) {
            return $this->RegionIdObject->CountryIdObject->Name;
        }
        return null;
    }
    
    public function getCheckInPeriod() {
        return $this->CheckInIdObject->Name;
    } 
    
    public function getGeojsonFile() {
        if (!is_null($this->RegionId)) {
            return $this->RegionIdObject->GeojsonFile;
        }
        return null;
    } 
    
    public function getAddress() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->GoogleMapGeoloc;
        }
        return null;
    }
    
    public function getLocationDetails() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->LocationDetails;
        }
        return null;
    }

    public function getPhotos() {
        
        $arrPhotosToReturn = [];
        $arrPhotos = explode(',', $this->GetVirtualAttribute("photos"));
   
        foreach ($arrPhotos as $photo) {
            if ($photo != '') {
                $arrPhotosToReturn[] = self::PHOTOS_PATH . '/' . self::PHOTOS_FULL_SIZE . '_' .$photo;
            }
        }
        
        return $arrPhotosToReturn;
        
    }
    
    public function getMainSliderPhotos() {
       
        $arrPhotosToReturn = [];
      
        for ($i=0; $i < self::MAIN_SLIDER_PHOTOS; $i++) {
            if (isset($this->photos[$i]) && $this->photos[$i] != '') {
                $arrPhotosToReturn[] = $this->photos[$i];
            }
        }
       
        return $arrPhotosToReturn;
        
    }
    
    public function getOverviewPhotos() {
        
        $arrPhotosToReturn = [];
      
        for ($i=self::MAIN_SLIDER_PHOTOS; $i < (self::MAIN_SLIDER_PHOTOS + self::OVERVIEW_PHOTOS); $i++) {
            if (isset($this->photos[$i]) && $this->photos[$i] != '') {
                $arrPhotosToReturn[] = $this->photos[$i];
            }
        }
        
        return $arrPhotosToReturn;
        
    }
    
    public function getAvailabilityPhoto() {
        
        $arrPhotosToReturn = [];

        if (isset($this->photos[(self::MAIN_SLIDER_PHOTOS + self::OVERVIEW_PHOTOS)])) {
            return $this->photos[(self::MAIN_SLIDER_PHOTOS + self::OVERVIEW_PHOTOS)];
        }
        
        return null;
        
    }
    
    public function getRemainingPhotos() {
        
        $arrRemainingPhotosToReturn = [];
        
        $initCount = (self::MAIN_SLIDER_PHOTOS + self::OVERVIEW_PHOTOS + 1);
        
        $nbRemainingPhotos = count($this->photos) - $initCount;

        for ($i=0; $i < $nbRemainingPhotos; $i++) {
            if (isset($this->photos[$initCount + $i]) && $this->photos[$initCount + $i] != '') {
                $arrRemainingPhotosToReturn[] = $this->photos[$initCount + $i];
            }
        }
        
        return $arrRemainingPhotosToReturn;
        
    }
    
    public function getAmenities() {
        
        $objAmenityCategory = null;
        $arrAmenities = [];
        
        $i = 0;
        
        if (!is_null($this->_join_property_amenity)) {
        
            foreach ($this->_join_property_amenity as $objJoinPropertyAmenity) {

                $strDescription = null;

                if (!is_null($objJoinPropertyAmenity->Description)) {
                    $strDescription = html_entity_decode(EncodingHelper::fixUTF8($objJoinPropertyAmenity->Description));
                }
                elseif (!is_null($objJoinPropertyAmenity->AmenityId) && !is_null($objJoinPropertyAmenity->AmenityIdObject->Description)) {
                    $strDescription = html_entity_decode(EncodingHelper::fixUTF8($objJoinPropertyAmenity->AmenityIdObject->Description));
                }
//
//                foreach ($this->_amenity_category as $_objAmenityCategory) {
//                    if (!is_null($objJoinPropertyAmenity->AmenityId) && $objJoinPropertyAmenity->AmenityIdObject->CategoryId == $_objAmenityCategory->Id) { 
//                        $objAmenityCategory = $_objAmenityCategory;
//                        break;
//                    }
//                    else {
//                        $objAmenityCategory = null;
//                    }
//                }
                
//              
//                if (!is_null($objAmenityCategory)) {

//                    $arrAmenities[$objAmenityCategory->Id]['category']['id'] = $objAmenityCategory->Id;
//                    $arrAmenities[$objAmenityCategory->Id]['category']['name'] = $objAmenityCategory->Name;
//
//                    if (!is_null($objAmenityCategory->Icon)) {
//                        $arrAmenities[$objAmenityCategory->Id]['category']['icon'] = 'icon icon-'.$objAmenityCategory->Icon;
//                    }
//
//                    $arrAmenities[$objAmenityCategory->Id]['category']['children'][] = [
//                        'id' => $objJoinPropertyAmenity->AmenityIdObject->Id,
//                        'name' => $objJoinPropertyAmenity->AmenityIdObject->Name,
//                        'description' => $strDescription
//                    ];
                    
                    $arrAmenities[$objJoinPropertyAmenity->Id] = [
                        'id' => $objJoinPropertyAmenity->AmenityIdObject->Id,
                        'name' => $objJoinPropertyAmenity->AmenityIdObject->Name,
                        'icon' => 'icon icon-'.$objJoinPropertyAmenity->AmenityIdObject->Icon,
                        'description' => $strDescription
                    ];
                    
                }

                $i++;
//
//            }
//        
        }
        
        return $arrAmenities;
        
    }
    
    public function getServices() {
        
        $arrServices = [];
        $objServiceCategory = null;
        
        $i = 0;
        
        if (!is_null($this->_join_property_service)) {
        
            foreach ($this->_join_property_service as $objJoinPropertyService) {

                $strDescription = null;

                if (!is_null($objJoinPropertyService->Description)) {
                    $strDescription = html_entity_decode(EncodingHelper::fixUTF8($objJoinPropertyService->Description));
                }
                elseif (!is_null($objJoinPropertyService->ServiceIdObject->Description)) {
                    $strDescription = html_entity_decode(EncodingHelper::fixUTF8($objJoinPropertyService->ServiceIdObject->Description));
                }

                foreach ($this->_service_category as $_objServiceCategory) {
                    if ($objJoinPropertyService->ServiceIdObject->CategoryId == $_objServiceCategory->Id) {
                        $objServiceCategory = $_objServiceCategory;
                        break;
                    }
                }

                $arrServices[$i]['service']['id'] = $objJoinPropertyService->ServiceId;
                $arrServices[$i]['service']['name'] = $objJoinPropertyService->ServiceIdObject->Name;
                $arrServices[$i]['service']['description'] = $strDescription;

                if (!is_null($objServiceCategory)) {
                    $arrServices[$i]['category']['id'] = $objServiceCategory->Id;
                    $arrServices[$i]['category']['name'] = $objServiceCategory->Name;
                }

                if (!is_null($objJoinPropertyService->ServiceIdObject->Image)) {
                    $arrServices[$i]['service']['image'] = self::PHOTOS_SERVICES_PATH . '/' . self::PHOTOS_FULL_SIZE . '_' .$objJoinPropertyService->ServiceIdObject->Image;
                }
//                elseif (!is_null($objJoinPropertyService->Image)) {
//                    $arrServices[$i]['service']['image'] = self::PHOTOS_SERVICES_PATH . '/' . self::PHOTOS_FULL_SIZE . '_' .$objJoinPropertyService->Image;
//                }

                $i++;
            }
        
        }
        
        return $arrServices;
    }

}
