<?php

namespace App\Helper;

class TransactionHelper {
    
    const RENTAL_CONDITIONS_PATH  = "/var/www/luxuryElocms/modules/30_specific30agents/rentalConditions.html";
    
    public static $lblStatus = array(
        0 => "archive",
        1 => "byrequest",
        2 => "byrequest approved",
        3 => "byrequest denied",
        4 => "pre-option",
        5 => "pre-option approved",
        6 => "pre-option denied",
        7 => "booked okay",
        8 => "booked waitfordeposit",
        9 => "booked waitforbalance",
    );
    
    public static $cellsStatus = array(
        0 => "byrequest",
        1 => "available",
        2 => "unavailable",
        3 => "onhold",
        4 => "booked"
    );
    
    public static $holdDuration = 432000;
    
    
    public static function getNetRental($objProperty, $arrivalDate, $departureDate) {
        $arrNetRentalCptPrices = \ModSpecific30Transactions::getTransactionNetRental($objProperty, $arrivalDate, $departureDate);
        return $arrNetRentalCptPrices['netRental'];
    }
    
    public static function getNetOwner($objProperty, $arrivalDate, $departureDate) {
        $arrNetOwnerCptPrices = \ModSpecific30Transactions::getTransactionNetOwner($objProperty, $arrivalDate, $departureDate);
        return $arrNetOwnerCptPrices['netOwner'];
    }
    
    public static function getNetExternal($objProperty, $arrivalDate, $departureDate) {
        $arrNetExternalCptPrices = \ModSpecific30Transactions::getTransactionNetExternal($objProperty, $arrivalDate, $departureDate);
    }
    
    public static function getCommission($netRental, $partnerCommission) {
        return \ModSpecific30Transactions::getCommission($netRental, $partnerCommission);
    }
    
    public static function getCostRental($netRental, $commission) {
        return $netRental - $commission;
    }
    
    public static function getMatchedPrices($objProperty, $arrivalDate, $departureDate) {
        $arrNetRentalCptPrices = \ModSpecific30Transactions::getTransactionNetRental($objProperty, $arrivalDate, $departureDate);
        return $arrNetRentalCptPrices['matchedPrices'];
    }
    
    public static function getApprovedDeniedDate() {
        return \QDateTime::FromTimestamp(time() + TransactionHelper::$holdDuration);
    }
    
    public static function formatComment($comment, $objPartner, $status) {
        
        $comment = $comment . "\n\n";
        $comment .= "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - \n\n";
        $comment .= "[" . date('m/d/Y H:i', time()) . "] Transaction Status set to " . self::$lblStatus[$status] . " (" . $objPartner . ")";
        
        return $comment;
            
    }
    
    public static function getStatusByType($type) {
        
        $status = null;
            
        switch ($type) {
            case 2 :
                // By Request
                $status = 1;
                break;
            case 3 :
                // Hold
                $status = 4;
                break;
        }
        
        return $status;
    }
    
    public static function saveInfos($objTransaction, $arrivalDate, $departureDate) {
        return \ModSpecific30Transactions::saveTransactionsInfos($objTransaction, $arrivalDate, $departureDate);
    }

    public static function isValidTransaction($arrAvailabilities, $arrTransactions, $arrivalDate, $departureDate, $type) {
        
        $isValid = false;

        foreach ($arrAvailabilities as $objAvailability) {

            for ($a = strtotime($arrivalDate); $a < strtotime($departureDate); $a = strtotime(date('Y-m-d', $a) . ' +1 day')) {
                
                if ($a >= strtotime($objAvailability->StartDate) && $a < strtotime($objAvailability->EndDate)) {

                    if ($objAvailability->Type == 0) {
                        if ($type == 2) {
                            // By Request
                            $isValid = true;
                        }
                    } elseif ($objAvailability->Type == 1) {
                        $isValid = true;
                    }
                }
            }
        }

        foreach ($arrTransactions as $objTransaction) {

            for ($a = strtotime($this->arrivalDate); $a < strtotime($this->departureDate); $a = strtotime(date('Y-m-d', $a) . ' +1 day')) {

                if ($a >= strtotime($objTransaction->DateArrival) && $a < strtotime($objTransaction->DateDeparture)) {

                    if ($objTransaction->Status != 4 || $objTransaction->Status != 5 || $objTransaction->Status < 7) {
                        $isValid = true;
                    }
                }
            }
        }
        
        return $isValid;
        
    }
    

    public static function getTransactionsPerMonth($arrTransactions, $currentYearMonth) {
        
        $arrTransactionsPerMonth = [];
        
        $currentYearMonth = explode("-", $currentYearMonth);
        $ndDayCurrentMonth = intval(date("t", mktime(0, 0, 0, $currentYearMonth[1], 1, $currentYearMonth[0])));

        foreach ($arrTransactions as $objTransaction) {

            $startDate = $objTransaction->DateArrival->PHPDate("Y-m-d");
            $startDateTab = explode('-', $startDate);

            $startYear = (int) $startDateTab[0];
            $startMonth = $startDateTab[1];
            $startDay = (int) $startDateTab[2];

            $endMonth = $objTransaction->DateDeparture->PHPDate("Y-m-d");
            $endDateTab = explode('-', $endMonth);

            $endYear = (int) $endDateTab[0];
            $endMonth = (int) $endDateTab[1];
            $endDay = (int) $endDateTab[2];


            // La réservation commence avant le mois et se termine apres ou pendant
            if (date("Y-m-d", mktime(0, 0, 0, $startMonth, $startDay, $startYear)) < date("Y-m-d", mktime(0, 0, 0, $currentYearMonth[1], 1, $currentYearMonth[0])) && date("Y-m-d", mktime(0, 0, 0, $endMonth, $endDay, $endYear)) >= date("Y-m-d", mktime(0, 0, 0, $currentYearMonth[1], 1, $currentYearMonth[0]))) {

                //Elle se termine après, je remplis mon mois en entier
                if (date("Y-m-d", mktime(0, 0, 0, $endMonth, $endDay, $endYear)) > date("Y-m-d", mktime(0, 0, 0, $currentYearMonth[1], $ndDayCurrentMonth, $currentYearMonth[0]))) {
                    for ($i = 1; $i <= $ndDayCurrentMonth; $i++)
                        $arrTransactionsPerMonth[$i - 1] = \ModSpecific30Transactions::transactionStatusToAvailability($objTransaction->Status);
                }
                //Elle se termine pendant, je remplis jusqu'au jour de fin
                else {
                    for ($i = 1; $i < $endDay; $i++) { //05/12/2013 remplacé <= par <
                        $arrTransactionsPerMonth[$i - 1] = \ModSpecific30Transactions::transactionStatusToAvailability($objTransaction->Status);
                    }
                }
            }

            // La réservation commence pendant le mois
            if (date("Y-m-d", mktime(0, 0, 0, $startMonth, $startDay, $startYear)) >= date("Y-m-d", mktime(0, 0, 0, $currentYearMonth[1], 1, $currentYearMonth[0])) && date("Y-m-d", mktime(0, 0, 0, $startMonth, $startDay, $startYear)) <= date("Y-m-d", mktime(0, 0, 0, $currentYearMonth[1], $ndDayCurrentMonth, $currentYearMonth[0]))) {

                //Elle se termine apres, je remplis mon mois en entier à partir du jour de début
                if (date("Y-m-d", mktime(0, 0, 0, $endMonth, $endDay, $endYear)) > date("Y-m-d", mktime(0, 0, 0, $currentYearMonth[1], $ndDayCurrentMonth, $currentYearMonth[0]))) {
                    for ($i = $startDay; $i <= $ndDayCurrentMonth; $i++)
                        $arrTransactionsPerMonth[$i - 1] = \ModSpecific30Transactions::transactionStatusToAvailability($objTransaction->Status);
                }
                // Elle se termine pendant, je remplis de mon jour de début jusqu'au jour de fin
                else {
                    for ($i = $startDay; $i < $endDay; $i++) //05/12/2013 remplacé <= par <
                    //QApplication::ExecuteJavaScript ("console.log('day : ".$i.",".ModSpecific30Transactions::transactionStatusToAvailability($objTransaction->Status)."');");
                        $arrTransactionsPerMonth[$i - 1] = \ModSpecific30Transactions::transactionStatusToAvailability($objTransaction->Status);
                }
            }
        }
        
        return $arrTransactionsPerMonth;
        
    }
    
}