<?php

namespace App\Helper;

class UploadFileHelper {
    
    const AUTH_FILE_EXTENSIONS = ['image/jpeg' => 'jpg', 'image/png' => 'png'];
    const MAX_FILE_SIZE = 1000000; // 1Mo
    
    public static function getErrors($file) {
        
        $errors = [];
        $blnToReturn = true;
        
        if (!in_array($file->getClientMimeType(), array_keys(self::AUTH_FILE_EXTENSIONS))) {
            $blnToReturn = false;
            $errors[] = "Autorized file types are : ". implode(', ', array_values(self::AUTH_FILE_EXTENSIONS));
        }
        
        if ($file->getClientSize() > self::MAX_FILE_SIZE) {
            $blnToReturn = false;
            $errors[] = "This file is more than ". (self::MAX_FILE_SIZE/1000000) ." MB.";
        }
        
        return $errors;
        
    }
    
}