
module.exports = {
    show: show,
    process: process,
    update: update
};

var $modal = $('#default-modal');


function update(type, params) {
    
    var jxhr = $.get('/transaction/update?' + params, function (data) {
    })
    .done(function() {
        window.location.href = window.location.href;
    })
    .setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
    
}

function show(type, params) {
    
    if (type == "overview") {
        var jxhr = $.get('/transaction/details?' + params, function (data) {
            $modal.find('.modal-title').text("Transaction details");
            $modal.find('.modal-body').html(data);
            $modal.modal('show');
        }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    }

    if (type == 1) {
        var jxhr = $.get('/transaction/pricing?' + params, function (data) {
            $modal.find('.modal-title').text("Transaction details");
            $modal.find('.modal-body').html(data);
            $modal.modal('show');
        }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    }

    if (type == 2) {
        var jxhr = $.get('/transaction/byrequest?' + params, function (data) {
            $modal.find('.modal-title').text("Transaction details");
            $modal.find('.modal-body').html(data);
            $modal.modal('show');
        }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    }

    if (type == 3) {
        var jxhr = $.get('/transaction/hold?' + params, function (data) {
            $modal.find('.modal-title').text("Transaction details");
            $modal.find('.modal-body').html(data);
            $modal.modal('show');
        }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    }

}

function process(type, params) {

    var jxhr = $.get('/transaction/process?' + params, function (data) {
    })
    .done(function() {
        window.location.href = '/partner/account';
    })
    .setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 

}


