<?php

namespace App\Vertuoz\Decorator;
use \InvalidArgumentException;

abstract class AbstractDecorator {

    private $component;

    const COMPONENT_CLASS = false;

    public function __construct($component) {
        $type = static::COMPONENT_CLASS;
        if ($type) {
            if ($component instanceof $type) {
                $this->component = $component;
                return;
            }
            if ($component instanceof self) {
                if ($type == static::COMPONENT_CLASS) {
                    $this->component = $component;
                    return;
                }
            }
        } else {
            // No component type defined, take any object:
            if (is_object($component)) {
                $this->component = $component;
                return;
            }
        }
        throw new InvalidArgumentException(sprintf(
                'Argument 1 passed to %s::%s must be an instance of %s or according decorator, %s given', __CLASS__, __FUNCTION__, $type, gettype($component)
        ));
    }

    public function __get($name) {
        return $this->component->$name;
    }

    public function __set($name, $value) {
        $this->component->$name = $value;
    }

    public function __isset($name) {
        return isset($this->component->$name);
    }

    public function __unset($name) {
        unset($this->component->$name);
    }

    public function __call($name, $arguments) {
        return call_user_func_array(array($this->component, $name), $arguments);
    }

}
