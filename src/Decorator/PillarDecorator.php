<?php

namespace App\Decorator;

use App\Vertuoz\Decorator\AbstractDecorator;
use App\Vertuoz\Helper\EncodingHelper;

class PillarDecorator extends AbstractDecorator {

    const COMPONENT_CLASS = '\ModSpecific30JoinPropertyPillar';
    const PHOTOS_PATH = 'http://rootcms.luxury-rentals.com/documents/30/specific30agents/pillars';

    public function __construct($component) {

        parent::__construct($component);
    }

    public function getPillar() {

        if (!is_null($this->PillarId)) {
            return $this->PillarIdObject;
        }

        return null;
    }
    
public function getName() {

        $name = null;

        if (!is_null($this->PillarId)) {
            return $this->PillarIdObject->Name;
        }

        return $name;
    }

    public function getDescription() {

        if (!is_null($this->Description)) {
            return html_entity_decode(EncodingHelper::fixUTF8($this->Description));
        } else {
            if (!is_null($this->PillarId) && !is_null($this->PillarIdObject->Description)) {
                return html_entity_decode(EncodingHelper::fixUTF8($this->PillarIdObject->Description));
            }
        }

        return null;
    }
    
    public function getImage() {
//        
//        if (!is_null($this->Image)) {
//            return self::PHOTOS_PATH . '/' . $this->Image;
//        } else {
            if (!is_null($this->PillarId) && !is_null($this->PillarIdObject->Image)) {
                return self::PHOTOS_PATH . '/' . $this->PillarIdObject->Image;
            }
//        }

        return null;
        
    }

}
