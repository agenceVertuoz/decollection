
module.exports = {
    create: create,
    edit: edit,
    process: process,
    remove: remove
};

var $modal = $('#default-modal');


function create() {
    
    var jxhr = $.get('/customer/create', function (data) {
        $modal.find('.modal-title').text("Customer");
        $modal.find('.modal-body').html(data);
        $modal.modal('show');
    })
    .setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
    
}


function edit(customerId) {
    
    var jxhr = $.get('/customer/edit?customerId=' + customerId, function (data) {
        $modal.find('.modal-title').text("Customer");
        $modal.find('.modal-body').html(data);
        $modal.modal('show');
    })
    .setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
    
}


function process(params) {
    
    var jxhr = $.get('/customer/process?' + params, function (data) {
    })
    .done(function() {
        $modal.modal('hide');
        setTimeout(function() {
            //window.location.href = window.location.href;
        }, 300);
    })
    .setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
    
}


function remove(customerId) {
    
    var jxhr = $.get('/customer/delete?customerId=' + customerId, function (data) {
    })
    .done(function() {
        $modal.modal('hide');
        setTimeout(function() {
            window.location.href = window.location.href;
        }, 300);
    })
    .setRequestHeader('X-Requested-With', 'XMLHttpRequest'); 
    
}