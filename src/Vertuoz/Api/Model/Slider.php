<?php

namespace App\Vertuoz\Api\Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Slider {

    function __construct($db, $appId) {
        $this->db = $db;
        $this->appId = $appId;
    }

    function getOne($params) {
        
        $params['siteId'] = $this->appId;

        $res = $this->getAll($params);
        if (is_array($res) && count($res) > 0) {
            $slideshow = $res[0];

            $slideshowSlides = $this->getSlides($slideshow["id"], $params['language']);

            $slideshow["slides"] = $slideshowSlides;

            return $slideshow;
        } else {
            return null;
        }
    }

    function getSlides($slideshowId, $language = null) {

        $sql = ""
                . "SELECT *"
                . " FROM mod_slideshowPicture "
                . " WHERE instanceId = $slideshowId AND isActivated = 1"
                . " ORDER BY position ASC "
                . " LIMIT 0,1000";

        $sql = "SELECT"
                    . " slide.file,"
                    . " slide.id,"
                    . " COALESCE(transInfo.value, slide.info) AS info,"
                    . " slide.instanceId,"
                    . " slide.isActivated,"
                    . " COALESCE(transLink.value, slide.link) AS link,"
                    . " COALESCE(transName.value, slide.name) AS name,"
                    . " slide.position,"
                    . " slide.targetBlankLink,"
                    . " COALESCE(transButton.value, slide.textlink) AS textlink"
                    . " FROM mod_slideshowPicture AS slide"
                    . " LEFT JOIN translation AS transName ON transName.sourceId = slide.id AND transName.code = 'modSlideshow3PictureName' AND transName.language = '".$language."'"
                    . " LEFT JOIN translation AS transLink ON transLink.sourceId = slide.id AND transLink.code = 'modSlideshow3PictureLink' AND transLink.language = '".$language."'"
                    . " LEFT JOIN translation AS transButton ON transButton.sourceId = slide.id AND transButton.code = 'modSlideshow3PictureButton' AND transButton.language = '".$language."'"
                    . " LEFT JOIN translation AS transInfo ON transInfo.sourceId = slide.id AND transInfo.code = 'modSlideshow3PictureInfo' AND transInfo.language = '".$language."'"
                    . " WHERE instanceId = " . $slideshowId
                    . " AND isActivated = 1"
                    . " ORDER BY position ASC"
                    . " LIMIT 0,1000";

        $results = $this->db->fetchAll($sql, array());

        return $results;
    }

    function getAll($params) {

        $queryFilter = null;
        $queryParams = array();
        $language = $params['language'];
        unset($params['language']);

        foreach ($params as $name => $value) {
            if (!is_null($value)) {
                $queryFilter .= " AND $name = ?";
                $queryParams[] = $value;
            }
        }

        $sql = ""
                . "SELECT *"
                . " FROM mod_slideshowInstance "
                . " WHERE 1 = 1 "
                . $queryFilter . " "
                . ""
                . "LIMIT 0,1000";

        $results = $this->db->fetchAll($sql, $queryParams);

        return $results;
    }

    /*
      function update($id, $dataToUpdate) {
      $result = $this->db->update("users", $dataToUpdate, array("id" => $id));
      return $result;
      }

      function delete($id) {

      $result = $this->db->delete("users", array("id" => (int) $id));
      return $result;
      }

      function create($data) {

      $result = $this->db->insert("users", $data);
      return $result;
      } */
}
