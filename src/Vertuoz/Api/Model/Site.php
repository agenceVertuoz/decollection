<?php

namespace App\Vertuoz\Api\Model;

class Site {

    public $id;
    public $language = '';
    public $language1 = '';
    public $language2 = '';
    public $language3 = '';
    public $language4 = '';
    public $language5 = '';
    public $name = '';
    public $nameLanguage2 = '';
    public $nameLanguage3 = '';
    public $nameLanguage4 = '';
    public $nameLanguage5 = '';
    public $metaDescription = '';
    public $metaDescriptionLanguage2 = '';
    public $metaDescriptionLanguage3 = '';
    public $metaDescriptionLanguage4 = '';
    public $metaDescriptionLanguage5 = '';
    protected $languagesAvailables = [];
    protected $names = [];
    protected $descriptions = [];
    protected $mail1 = '';
    protected $mail2 = '';
    protected $isOnline = true;

    function __construct($db) {
        $this->db = $db;
    }

    function hydrate($id) {

        /*
        Requête SQL
         */
        
        $sql = "SELECT `id`,
                `name`,
                `nameLanguage2`,
                `nameLanguage3`,
                `nameLanguage4`,
                `nameLanguage5`,
                `url`,
                `metaKeywords`,
                `metaDescription`,
                `metaKeywordsLanguage2`,
                `metaDescriptionLanguage2`,
                `metaKeywordsLanguage3`,
                `metaDescriptionLanguage3`,
                `metaKeywordsLanguage4`,
                `metaDescriptionLanguage4`,
                `metaKeywordsLanguage5`,
                `metaDescriptionLanguage5`,
                `mail1`,
                `mail2`,
                `language1`,
                `language2`,
                `language3`,
                `language4`,
                `language5`,
                `errorUrl`,
                `isOnline`
                 FROM site "
                . " WHERE site.id = ?";

        $result = $this->db->fetchAssoc($sql, array((int) $id));
    
        /*
        Langues : defaultLanguage et $languages
         */
        
         if(preg_match("/[a-zA-Z]{2}/", $result['language1']) == 0) {
             throw new \Exception("Erreur de config back-office : aucune langue par défaut", 500);
        }

        for($i = 1; $i <= 5; $i++) { //5 champs language dispo
            
            //$suffix = ($i == 1) ? '' : $i;
            if(preg_match("/[a-zA-Z]{2}/", $result['language'.$i]) > 0)
                $this->languagesAvailables[] = strtoupper($result['language'.$i]);
        }

        $this->id = $result['id'];
        $this->language = reset($this->languagesAvailables);
        
        $this->name = $result['name'];
        $this->nameLanguage2 = $result['nameLanguage2'];
        $this->nameLanguage3 = $result['nameLanguage3'];
        $this->nameLanguage4 = $result['nameLanguage4'];
        $this->nameLanguage5 = $result['nameLanguage5'];
        
        $this->metaDescription = $result['metaDescription'];
        $this->metaDescriptionLanguage2 = $result['metaDescriptionLanguage2'];
        $this->metaDescriptionLanguage3 = $result['metaDescriptionLanguage3'];
        $this->metaDescriptionLanguage4 = $result['metaDescriptionLanguage4'];
        $this->metaDescriptionLanguage5 = $result['metaDescriptionLanguage5'];
        
        $this->language1 = $result['language1'];
        $this->language2 = $result['language2'];
        $this->language3 = $result['language3'];
        $this->language4 = $result['language4'];
        $this->language5 = $result['language5'];
        
        /*
        Names
        */ 
        
        for($i = 1; $i <= count($this->languagesAvailables); $i++) { //Autant de titre de site que de languages paramétrés
            
            $suffix = ($i == 1) ? '' : "Language".$i;
            $this->names[$this->languagesAvailables[$i-1]] = $result['name'.$suffix];
        }

        /*
        Description
        */ 
        
        for($i = 1; $i <= count($this->languagesAvailables); $i++) {
            
            $suffix = ($i == 1) ? '' : "Language".$i;
            $this->descriptions[$this->languagesAvailables[$i-1]] = $result['metaDescription'.$suffix];
        }

        /*
        Autres paramètres
         */
        
        $this->mail1    = $result['mail1'];
        $this->mail2    = $result['mail2'];
        $this->isOnline = ($result['isOnline'] == 1);

        return $this;   
    }
    
    public function getLanguage() {
        return $this->language;
    }
    
    public function setLanguage($language) {
        return $this->language = strtoupper($language);
    }

    public function getLanguagesAvailables() {
        return $this->languagesAvailables;
    }
    
    public function getLanguageId($locale) {
        
        if (strtolower($locale) == strtolower($this->language1))
            return 1;
        if (strtolower($locale) == strtolower($this->language2))
            return 2;
        if (strtolower($locale) == strtolower($this->language3))
            return 3;
        if (strtolower($locale) == strtolower($this->language4))
            return 4;
        if (strtolower($locale) == strtolower($this->language5))
            return 5;
        
        return 0;
        
    }
    
}
