<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Helper\PartnerFormHelper;

class PartnerPasswordType extends AbstractType
{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $partner = $options['partner'];
        
        $builder
           
            ->add('current_password', PasswordType::class, array(
                'required' => true,
                'attr' => array('autocomplete' => 'off')
            )) 
                
            ->add('new_password', PasswordType::class, array(
                'required' => true,
                'attr' => array('autocomplete' => 'off')
            ))
                
            ->add('new_password_confirm', PasswordType::class, array(
                'required' => true,
                'attr' => array('autocomplete' => 'off')
            ))
                
            ->add('save', SubmitType::class, array('label' => 'Save'))
        
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('partner');
    }
    
}

