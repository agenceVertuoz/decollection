<?php

namespace App\Helper;

use App\Helper\TransactionHelper;

class CalendarHelper {

    public function getHtml($arrAvailabilitiesPerMonth, $currentDate) {
   
        $currentDateExploded = explode('-',$currentDate);
        $currentDateYear     = $currentDateExploded[0];
        $currentDateMonth    = $currentDateExploded[1];
     
        $tableau = ["0", "1", "2", "3", "4", "5", "6", "0"];
        $ndDay   = date("t", mktime(0, 0, 0, date('n', strtotime($currentDate)), 1, date('Y', strtotime($currentDate))));
        $step    = 0;
        $indexe  = 1;
        
        $calendarString = "";
        $calendarString .=  "<div id='InsideContainer' class='InsideContainer'>";
        $calendarString .= "<div class=\"Month\">";
        $calendarString .= date('F', strtotime($currentDate));
        $calendarString .= "</div>";
        
        //Calendar Header
        $calendarString .= "<div class='Header'>Lun</div><div class='Header'>Mar</div><div class='Header'>Mer</div><div class='Header'>Jeu</div><div class='Header'>Ven</div><div class='Header'>Sam</div><div class='Header'>Dim</div><div class='EndClear'><!-- --></div>";

        // Tant que l'on n'a pas
        //affecté tous les jours du mois traité
        while ($step < $ndDay) { 
            // Si le jour calendrier == jour de la semaine en cours
            if (date("w", mktime(0, 0, 0, date('n', strtotime($currentDate)), 1 + $step, date('Y', strtotime($currentDate)))) == $tableau[$indexe]) {
                
                $class = "";
                
                // SI le jour est celui d'aujourd'hui
                if (date("Y-m-d", mktime(0, 0, 0, date('n', strtotime($currentDate)), 1 + $step, date('Y', strtotime($currentDate)))) == date("Y-m-d")) {
                    $dayDisplayed = date("j", mktime(0, 0, 0, date('n', strtotime($currentDate)), 1 + $step, date('Y', strtotime($currentDate))));
                    $class .= 'day current';
                    $dayDisplayed = "<span class='DayNow'>".$dayDisplayed."</span>";
                }
                else {
                    $dayDisplayed = date("j", mktime(0, 0, 0, date('n', strtotime($currentDate)), 1 + $step, date('Y', strtotime($currentDate))));
                    $class .= 'day regular';
                }
                
                if (isset($arrAvailabilitiesPerMonth[$step])) {
                    $class .= " ".TransactionHelper::$cellsStatus[$arrAvailabilitiesPerMonth[$step]];
                }

                $class = " class=\"".$class."\"";
                $id = " id=\"".$currentDateMonth."_day_".$step."\"";

                $hoteGotoResa = "";
                //if($this->eventsFound == true) {
                    //$hoteGotoResa = "onclick=\"modAgendaShowEvents('".Date("Y-m-d", mktime(0, 0, 0, $this->getMonth($currentDate), 1 + $step, $this->getYear($currentDate)))."','".$_GET['cssInstanceId']."','".$_GET['cssClass']."','".$_GET['ajaxUrl']."');\"";
                //}

                $calendarString .= "<div ".$id." ".$class." ".$hoteGotoResa."><span>".$dayDisplayed."</span></div>";
                $step++;
            }
            else $calendarString .= "<div class='DayEmpty'></div>";

            if ($indexe == 7 && $step < $ndDay) {
                $indexe = 1;
                //$calendarString.="<div style='clear:both;height:0px;line-height:0px;padding:0px;margin:0px;'><!-- --></div>";
            }
            else {
                $indexe++;
            }
        }
        $calendarString .=  "<div class='EndClear'><!-- --></div></div>";

        return $calendarString;
        
    }
    
}
