
module.exports = {
    show: show,
};

var $modal = $('#default-modal');

function show(amenityId, propertyId) {
    var jxhr = $.get('amenity/show?amenityId=' + amenityId + '&propertyId=' + propertyId, function (data) {
        $modal.find('.modal-title').text("Details");
        $modal.find('.modal-body').html(data);
        $modal.modal('show');
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
}


