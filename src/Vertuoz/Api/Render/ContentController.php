<?php

namespace App\Vertuoz\Api\Render;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Vertuoz\Api\Helper\ContentHelper;
use GuzzleHttp\Exception;

class ContentController extends Controller
{
    
    /**
     * @Route("/render-content-collection", name="render_content_collection", methods={"GET"})
     */
    public function renderContentCollection(Request $request)
    {
        
        $apiClient = $this->get('api.client');
        $debug = $this->getParameter('kernel.debug');
        $twigData = [];

        $collectionTplName = $request->query->get('collectionTplName', 'default');
        $areaName = $request->query->get('areaName');
        $tplName = $request->query->get('tplName', 'default');
        $noElementText = $request->query->get('noElementText', 'Aucun élément à afficher');
        $params = $request->query->get('params', []);
        
        if (!$areaName) {
            throw new BadRequestHttpException("Area is missing");
        }
        
        $params['areaName'] = $areaName;
     
        $baseViewPath = "api/content-collection";
        $viewPath = $baseViewPath . "/" . $collectionTplName;
 
        try {
            
            $contentHelper = new ContentHelper($apiClient);
            $contents = $contentHelper->listContents($params);
            
        } catch (ClientException $e) {}
    
        if (empty($contents) && $debug) {
            $viewPath = "api/error/notfound.html.twig";
            return $this->render($viewPath, array("message" => sprintf("Contenus zone <strong>%s</strong> non trouvé", $params['areaName'])));
        }
        
        $twigData["contents"] = $contents;
        $twigData['areaName'] = $areaName;
        $twigData["params"] = $params;
        $twigData["tplName"] = $tplName;
        $twigData["noElementText"] = urldecode($noElementText);
      
        return $this->render($viewPath . '.html.twig', $twigData);
    }
    
    
    /**
     * @Route("/render-content", name="render_content", methods={"GET"})
     */
    public function renderContent(Request $request)
    {
        
        $apiClient = $this->get('api.client');
        $debug = $this->getParameter('kernel.debug');
        $twigData = [];

        $id = $request->query->get('id');
        $code = $request->query->get('code');
        $tplName = $request->query->get('tplName', 'default');
        $titleTagName = $request->query->get('titleTagName', 'div');

        $baseViewPath = "api/content";
        $viewPath = $baseViewPath . "/" . $tplName;
  
        try {
            
            $contentHelper = new ContentHelper($apiClient);
            
            if ($code) {
                
                $content = $contentHelper->getOneByCode($code);
                if (empty($content) && $debug) {
                    $viewPath = "api/error/notfound.html.twig";
                    return $this->render($viewPath, array("message" => sprintf("Contenu code <strong>%s</strong> non trouvé", $code)));
                }
                
            }
            elseif ($id) {
                
                $content = $contentHelper->get($id);
                if (empty($content) && $debug) {
                    $viewPath = "api/error/notfound.html.twig";
                    return $this->render($viewPath, array("message" => sprintf("Contenu id <strong>%s</strong> non trouvé", $id)));
                }
                
            }
            
        } catch (ClientException $e) {}
        
        $twigData["content"] = $content;
        $twigData["titleTagName"] = $titleTagName;
      
        return $this->render($viewPath . '.html.twig', $twigData);
    }
    
}
