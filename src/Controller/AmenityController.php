<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class AmenityController extends Controller {

    /**
     * @Route("/amenity/show", name="amenity_show")
     */
    public function show(Request $request) {

        $twigData = [];

        $amenityId = $request->query->get('amenityId');
        $propertyId = $request->query->get('propertyId');
        
        $objJoinAmenityProperty = $this->get('amenity.service')->loadAmenityByPropertyId($amenityId, $propertyId);
        
        return $this->render('amenity/show.html.twig', [
            'name' => $objJoinAmenityProperty->getName(),
            'description' => $objJoinAmenityProperty->getDescription(),
            'icon' => $objJoinAmenityProperty->getIcon()
        ]);
        
    }

}



