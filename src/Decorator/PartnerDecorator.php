<?php

namespace App\Decorator;

use App\Vertuoz\Decorator\AbstractDecorator;
use App\Vertuoz\Helper\UriHelper;
use App\Vertuoz\Helper\EncodingHelper;
use App\Service\PropertyService;
use App\Service\CustomerService;
use App\Helper\TransactionHelper;

class PartnerDecorator extends AbstractDecorator {

    const COMPONENT_CLASS = '\ModSpecific30Partner';
    const PHOTOS_URL = 'http://rootcms.luxury-rentals.com/documents/30/specific30agents/logo_partner';
    const PHOTOS_PATH = '/var/www/luxuryElocms/documents/30/specific30agents/logo_partner';

    public $customers;
    public $fullname;
    public $percentCommission;
    protected $_arrCustomers;
    protected $_arrTransactions;

    public function __construct($component, array $arrCustomers = null, $arrTransactions = null) {

        parent::__construct($component);

        $this->_arrCustomers = $arrCustomers;
        $this->_arrTransactions = $arrTransactions;

        $this->customers = $this->getCustomers();
        $this->fullname = $this->getFullname();
        $this->percentCommission = $this->getPercentCommission();
    }

    public function __toString() {
        return $this->getFullname();
    }

    public function getId() {
        return $this->Id;
    }

    public function getLogo() {
        if (!is_null($this->Logo)) {
            return self::PHOTOS_URL . '/' . $this->Logo;
        }
        return null;
    }

    public function getLogin() {
        return $this->Login;
    }

    public function getPassword() {
        return $this->Password;
    }

    public function getLastname() {
        return $this->AddressIdObject->Lastname;
    }

    public function getFirstname() {
        return $this->AddressIdObject->Firstname;
    }

    public function getCompany() {
        return $this->AddressIdObject->Company;
    }

    public function getAddress() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->Address;
        }
        return null;
    }

    public function getPostalCode() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->PostalCode;
        }
        return null;
    }

    public function getCity() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->City;
        }
        return null;
    }

    public function getState() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->State;
        }
        return null;
    }

    public function getEmail() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->AutomaticDefaultEmail;
        }
        return null;
    }

    public function getCountryId() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->CountryId;
        }
        return null;
    }

    public function getHomePhone() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->HomePhone;
        }
        return null;
    }

    public function getOfficePhone() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->OfficePhone;
        }
        return null;
    }

    public function getOfficeCellular() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->OfficeCellular;
        }
        return null;
    }

    public function getHomeCellular() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->HomeCellular;
        }
        return null;
    }

    public function getOfficeFax() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->OfficeFax;
        }
        return null;
    }

    public function getHomeFax() {
        if (!is_null($this->AddressId)) {
            return $this->AddressIdObject->HomeFax;
        }
        return null;
    }

    public function getFullname() {

        $fullname = "";

        if (strlen($this->AddressIdObject->Lastname) > 0) {
            $fullname = $this->AddressIdObject->Lastname;
            if ($this->AddressIdObject->Firstname) {
                $fullname .= " " . $this->AddressIdObject->Firstname;
            }
        } elseif (strlen($this->AddressIdObject->Company) > 0) {
            $fullname = $this->AddressIdObject->Company;
        } else {
            $fullname = "No name / company";
        }

        return $fullname;
    }

    public function getTransactions() {

        $arrTransactions = [];

        if (!is_null($this->_arrTransactions)) {

            foreach ($this->_arrTransactions as $objTransaction) {

                $objProperty = PropertyService::decorate($objTransaction->PropertyIdObject);
                $objCustomer = CustomerService::decorate($objTransaction->CustomerIdObject);
                $statusClass = TransactionHelper::$lblStatus[$objTransaction->Status];
                $currency = $objProperty->CurrencyIdObject->Code;

                $arrTransactions[] = [
                    'id' => $objTransaction->Id,
                    'property' => $objProperty,
                    'customer' => $objCustomer,
                    'currency' => $currency,
                    'nbAdults' => $objTransaction->NbAdults,
                    'nbChildren' => $objTransaction->NbChildren,
                    'dateArrival' => $objTransaction->DateArrival,
                    'dateDeparture' => $objTransaction->DateDeparture,
                    'status' => $objTransaction->Status,
                    'statusClass' => $statusClass,
                    'netRental' => $objTransaction->NetRental,
                    'commission' => $objTransaction->Commission,
                ];
            }
        }

        return $arrTransactions;
    }

    public function getCustomers() {

        $arrCustomers = [];

//        if (!is_null($this->_arrCustomers)) {
//
//            foreach ($this->_arrCustomers as $objCustomer) {
//
//                $fullname = $this->getFullname();
//
//                $arrCustomers[] = [
//                    'id' => $objCustomer->Id,
//                    'name' => $fullname
//                ];
//                
//            }
//        }

        if (!is_null($this->_arrCustomers)) {

            foreach ($this->_arrCustomers as $objCustomer) {

                $objDecoratedCustomer = CustomerService::decorate($objCustomer);

                $arrCustomers[] = [
                    'id' => $objDecoratedCustomer->Id,
                    'name' => $objDecoratedCustomer->getFullname(),
                    'city' => $objDecoratedCustomer->getCity(),
                    'country' => $objDecoratedCustomer->getCountry(),
                    'phone' => $objDecoratedCustomer->getPhone()
                ];
            }
        }

        return $arrCustomers;
    }

    public function getPercentCommission() {
        return (!is_null($this->PercentCommission)) ? $this->PercentCommission : 0;
    }

}
