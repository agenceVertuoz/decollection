<?php

namespace App\Service;
use App\Decorator\RateDecorator;

class RateService {

    public function loadAllByPropertyId($propertyId) {
        
        $arrPrices = \ModSpecific30Price::QueryArray(
            \QQ::AndCondition(
               \QQ::Equal(\QQN::ModSpecific30Price()->PropertyId, $propertyId),
               \QQ::GreaterOrEqual(\QQN::ModSpecific30Price()->EndDate, \QDatetime::FromTimestamp(strtotime(date("Y-m-d")." 00:00:00")))
            ),
            \QQ::Clause(
                \QQ::OrderBy(
                    \QQN::ModSpecific30Price()->StartDate, true
                )
            )
        );
        
        $arrDecoratedPrices = self::decorateCollection($arrPrices);

        return $arrDecoratedPrices;
        
    }
    
    public static function decorateCollection(array $arrPrices) {

        $arrDecoratedPrices = [];
        
        foreach ($arrPrices as $objPrice) {
            $objNewPrice = self::decorate($objPrice);
            $arrDecoratedPrices[] = $objNewPrice;
        }
        
        return $arrDecoratedPrices;
        
    }
    
    public static function decorate($objPrice) {

        $objDecoratedPrices = new RateDecorator($objPrice);

        return $objDecoratedPrices;
        
    }

}
