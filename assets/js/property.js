// CSS
// --------------------

//require('../scss/app.scss');

const jQueryLazy = require('jquery-lazy');
const Fancybox = require('@fancyapps/fancybox');


// Services
// --------------------

const RatesServices = require('./rates-services');
const AvailabilitiesServices = require('./availabilities-services');
const AmenitiesServices = require('./amenities-services');
const ServiceServices = require('./service-services');
const TransactionServices = require('./transaction-services');

global.TransactionServices = TransactionServices;
global.AvailabilitiesServices = AvailabilitiesServices;


$('body').addClass('noscroll');



// Fancybox gallery
// --------------------

$('[data-fancybox="gallery"]').fancybox({
    loop: true,
    buttons: [
        "zoom",
        "slideShow",
        "fullScreen",
        "thumbs",
        "close"
    ],
    fullScreen : {
        autoStart: true
    },
    thumbs : {
        autoStart : true
    }
});



// First letter color
// --------------------

$.each($('.each-word'), function() {
    var word = $(this).html();
    var index = word.search("De Collection");
    if (index !== -1) {
        $(this).html(word.substring(0, index) + '<span class="first-letter">' + word.substring(index, word.length) + '</span>');
    }
});



// Images lazy load
// --------------------

var $imgsToPreload = [];
var imgsPreloaded = 0;

$.each($('[data-src]'), function () {
    $imgsToPreload.push($(this));
});

$("[data-src]").Lazy({
    threshold: 100000,
    afterLoad: function(element) { 
        imgsPreloaded++;
        var progressW = $('.progress').width();
        var progress = progressW * (imgsPreloaded / $imgsToPreload.length);
        $('.progress .progress-bar').width(progress);
        
    },
    onError: function() {
        imgsPreloaded++;
    },
    onFinishedAll: function() {
       setTimeout(function () {
            hideLoader();
            setTimeout(function () {
                bindAnimations();
                bindPlugins();
            }, 300);
        }, 300);
    }
    
 });
        


// Smooth scroll to anchor
// --------------------

$('[data-scroll-to]').click(function (e) {

    e.preventDefault();

    var el = $(this).data('scroll-to');
    var offset = $(this).data('offset') || 0;

    $('html, body').animate({
        scrollTop: $(el).offset().top + offset
    }, 1000, 'easeOutCubic');

});


// Navbar - Headroom.js
// --------------------

var nav = document.querySelector("nav");
var headroom = new Headroom(nav);
headroom.init();


/*
 .on('slideChange', function () {
 $('.services-list > li').removeClass('active');
 $('.services-list > li').eq(this.activeIndex).addClass('active');
 });
 
 $('.services-list > li').eq(0).addClass('active');
 
 $('.services-list > li > div > a').on('click', function(e) {
 e.preventDefault();
 var to = $(this).data('slide-to');
 servicesSlider.slideTo(to -1);
 });
 */

// Datepicker
// --------------------

$('#transaction_form .datepicker').datepicker({
    dropupAuto: false,
    format: 'yyyy-mm-dd',
    startDate: '+0d',
    endDate: '+365d',
    todayHighlight: true,
    weekStart: 0,
    minViewMode: 'month',
    startView: 'month'
});

var $modal = $('#default-modal');


// Button dropdown - Property
// --------------------

$('#transaction_form .dropdown-type .dropdown-item').on('click', function (e) {
    e.preventDefault();
    var type = $(this).data('type');
    $('#transaction_form .dropdown-type .dropdown-item').removeClass('selected');
    $(this).addClass('selected');
    $('#transaction_type').val(type);
    $('#transaction_form').find('button[type=submit]').trigger('click');
});

$('#transaction_form').on('submit', function (e) {

    e.preventDefault();

    $selectedItem = $(this).find('.dropdown-item.selected');
    var params = $(this).serialize();
    var type = $selectedItem.data('type');

    TransactionServices.show(type, params);

});


$('.property-details [data-amenity-id]').on('click', function (e) {

    e.preventDefault();
    var amenityId = $(this).data('amenity-id');
    var propertyId = $(this).data('property-id');

    AmenitiesServices.show(amenityId, propertyId);

});

$('.property-services [data-service-id]').on('click', function (e) {

    e.preventDefault();
    var serviceId = $(this).data('service-id');
    var propertyId = $(this).data('property-id');

    ServiceServices.show(serviceId, propertyId);

});



// Rates
// --------------------

$('.view-rates').on('click', function (e) {
    e.preventDefault();
    var propertyId = $(this).data('property-id');
    RatesServices.getRatesByPropertyId(propertyId);
});


// Availabilities
// --------------------

$('.view-availabilities').on('click', function (e) {
    e.preventDefault();
    var propertyId = $(this).data('property-id');
    var year = (new Date()).getFullYear();
    AvailabilitiesServices.getAvailabilitiesByPropertyId(propertyId, year);
});


$(window).on('load', function() {
    
    
$.each($('.parallax-element'), function() {
    
    var $parallaxElement = $(this);
    var parallaxTop = $parallaxElement.offset().top;
    var parallaxStart = parallaxTop * .8;
    var originalTop = parseInt($parallaxElement.css('top')) || 0;


    $(window).scroll(function() {
            
//        if ($parallaxElement.hasClass('form-container')) {
//console.log(originalTop);
//    }
        if (($(this).scrollTop()) >= parallaxStart) {
          $parallaxElement.css({
            'top': originalTop -(($(this).scrollTop() - parallaxStart) / 4) + "px"
          });
        }
      });
    
});
    
});



function hideLoader() {
    $('.loader').fadeOut();
    $('body').removeClass('noscroll');
}


  

function bindAnimations() {

    // ScrollReveal
    // --------------------

    ScrollReveal().reveal('.reveal', {delay: 800, duration: 1200});
//
//    ScrollReveal().reveal('.reveal', {delay: 1000, duration: 1200});
//    ScrollReveal().reveal('.reveal-property-overview-1', {delay: 0, duration: 600});
//    ScrollReveal().reveal('.reveal-property-overview-2', {delay: 600, duration: 600});

}

function bindPlugins() {


    // Slider - Main
    // --------------------

    var mainSlider = new Swiper('.main-slider', {
        loop: true,
        autoplay: {
            delay: 5000,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        pagination: {
            el: '.main-slider-pagination',
            clickable: true
        },
        on: {
            init: function () {
                this.$el.addClass('reveal');
                $('.main-hover').addClass('reveal');
            },
        }
    });



    // Services
    // --------------------
//
//    var servicesSlider = new Swiper('.services-slider', {
//        /*loop: false,*/
//        loo: true,
//        autoplay: {
//            delay: 5000,
//        },
//        pagination: {
//            el: '.main-slider-pagination',
//            clickable: true
//        },
//        on: {
//            init: function () {
//                this.$el.addClass('reveal');
//            },
//        }
//    });
    

}