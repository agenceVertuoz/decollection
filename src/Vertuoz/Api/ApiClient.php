<?php

namespace App\Vertuoz\Api;

use GuzzleHttp\Client;

class ApiClient {

    protected $apiVersion;
    protected $path;
    protected $guzzleClient;

    /**
     * 
     * @param \GuzzleHttp\Client $guzzleClient
     * @param type $apiVersion
     */
    public function __construct(Client $guzzleClient, $apiPath = '') {
        
        $this->guzzleClient = $guzzleClient;
        $this->path = $apiPath;
        
    }

    /**
     * 
     * @param string $url
     * @return type
     */
    public function get($url) {
        $response = $this->guzzleClient->get($this->path.$url)->getBody()->getContents();
        
        return json_decode($response, true);
    }

    /**
     * 
     * @param string $url
     * @return type
     */
    public function post($url, $data) {
        
        $response = $this->guzzleClient->post($this->path.$url, [
            'form_params' => $data
        ])->getBody()->getContents();

        return json_decode($response, true);
    }

}
