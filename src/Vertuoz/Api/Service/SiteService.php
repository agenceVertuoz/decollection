<?php

namespace App\Vertuoz\Api\Service;

use App\Vertuoz\Api\Model\Site AS SiteModel;

class SiteService {

    protected $db;
    protected $appId;
    protected $siteModel = null;


    public function __construct($db, $appId) {
        
        $this->db = $db;
        $this->appId = $appId;
        $this->siteModel = new SiteModel($this->db); 
    }
    
    /*
     * @return Vertuoz\EloCMSv2\Model\Site
     */
    public function get() {
        
        return $this->siteModel->hydrate($this->appId);
        //return $this->siteModel->hydrate(\Vertuoz\ApiContext\ApiContext::getCurrentSiteId());

    }

}
