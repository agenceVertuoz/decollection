<?php

namespace App\Service;
use App\Decorator\AmenityDecorator;

class AmenityService {

    public function loadAmenityByPropertyId($amenityId, $propertyId) {
        
        $objAmenity = \ModSpecific30JoinAmenityProperty::QuerySingle(
            \QQ::AndCondition(
                \QQ::Equal(\QQN::ModSpecific30JoinAmenityProperty()->PropertyId, $propertyId),
                \QQ::Equal(\QQN::ModSpecific30JoinAmenityProperty()->AmenityId, $amenityId)
            )
        );
        
        $objDecoratedAmenity = self::decorate($objAmenity);

        return $objDecoratedAmenity;
        
    }
    
    
    public static function decorate($objAmenity) {

        $objDecoratedAmenity = new AmenityDecorator($objAmenity);

        return $objDecoratedAmenity;
        
    }

}
