<?php

namespace App\Vertuoz\Api\Model;

use App\Vertuoz\Helper\EncodingHelper as Encoding;


class Content {

    function __construct($db, $appId) {
        $this->db = $db;
        $this->appId = $appId;
    }

    function update($id, $queryParams) {

        if(count($queryParams) > 0) {

            $strSet = "" ;
            foreach ($queryParams as $name => $value) {
                $strSet .= $name." = '".$value."', " ;
            }
            $strSet = substr($strSet, 0 , -2) ;

            // Requête complète
            $sql = "UPDATE content
            SET
            ".$strSet."

            WHERE
            id = ".$id."
            " ;

            $count = $this->db->executeUpdate($sql);

            return $count ;
        }
        else return null ;
    }

    function getOne($params) {

        $res = $this->getAll($params);
        if (is_array($res) && !is_null($res)) {
            return $res[0];
        } else {
            return null;
        }
    }

    function getAll($params) {

        $queryFilter = null;
        $queryParams = array();

        $limitFilter = null ;
        $limitValue  = null ;
        $startValue  = null ;
        $order       = " ORDER BY position" ;
        $direction   = "ASC" ;
        $language    = "";

        $extraFilters = null ;

        foreach ($params as $name => $value) {
            if (!is_null($value)) {

                switch($name) {

                    // Filtres autorisés

                    default :
                    $name = "content.".str_replace("content.","",$name);
                    $queryFilter  .= " AND $name = ?";
                    $queryParams[] = $value;
                    break ;

//                    //Code - pour eviter ambiguous field dans la requete avec la trad
//                    case "code" :
//                    $queryFilter  .= " AND content.code = ?";
//                    $queryParams[] = $value;
//                    break ;
//
//                    //Areaname - pour eviter ambiguous field dans la requete avec la trad
//                    case "areaName" :
//                    $queryFilter  .= " AND content.areaName = ?";
//                    $queryParams[] = $value;
//                    break ;

                    // Pour filtrer sur les extra_fields (Voir plus bas)
                    case "extrafield" :
                    if(is_array($value)) {
                        $extraFilters = $value ;
                    }
                    break ;

                    // Limite
                    case "limit" :
                    $limitValue = $value ;
                    break ;

                    // Limit start
                    case "start" :
                    $startValue = $value ;
                    break ;

                    // Order
                    case "order" :
                    $order = " ORDER BY ".$value ;
                    break ;

                    // Direction
                    case "direction" :
                    $direction = $value ;
                    break ;

                    case "language" :
                    $language = $value;
                    break;
                
                    case "keyword" :
                    $value;
                        $arrayFieldsSearch = array("title","introduction","content");
                        $queryFilter .= " AND (";
                        $orClause = [];
                        foreach($arrayFieldsSearch as $field)
                        {
                            $orClause[] = "COALESCE(contentTrans.$field, content.$field) like '%".$value."%'";
                            
                        }
                        
                        $queryFilter .= implode(" OR ",$orClause);
                        $queryFilter .= " )";
                        $queryFilter .= " AND (content.isListed IS NULL || content.isListed = 1) ";
                   // $queryParams[] = $value;
                    break;

                }
            }
        }


        // Construction du filtre limite
//        if(!is_null($startValue) && !is_null($limitValue)) {
//            $limitFilter = "LIMIT ".$startValue." , ".$limitValue ;
//        }
//        elseif(!is_null($limitValue)) {
//            $limitFilter = "LIMIT ".$limitValue ;
//        }
//        else {
//            $limitFilter = "LIMIT 0,1000" ;
//        }
        
        $limitFilter = "LIMIT 999";


        // Requête complète
        $sql = ""
        . "SELECT content.id"
        . ", content.siteId"
        . ", content.areaName"
        . ", COALESCE(contentTrans.language,content.language) AS language"
        . ", content.code"
        . ", COALESCE(contentTrans.title, content.title) AS title"
        . ", COALESCE(transSubtitle.value, content.subtitle) AS subtitle"
        . ", COALESCE(transIntro.value, content.introduction) AS introduction"
        . ", COALESCE(contentTrans.content,content.content) AS content"
        . ", content.position"
        . ", content.displayExtractInFullView"
        . ", content.pageUrl"
        . ", COALESCE(readMoreTrans.value,content.readMoreLink) AS readMoreLink"
        . ", content.date"
        . ", content.dateDebut"
        . ", content.dateFin"
        . ", COALESCE(pic.file,'') AS highlightpic"
        . ", area.config AS areaConfig"
       // . ", COALESCE(mod_extrafields.fieldsLanguage1,'') AS fieldsConfig"
        . ",(select mod_extrafields.fieldsLanguage1 from mod_extrafields where mod_extrafields.siteId = content.siteId) AS fieldsConfig"        
        . " FROM content AS content"
        . " LEFT JOIN area ON content.areaName = area.alias AND area.siteId = content.siteId"
        . " LEFT JOIN mod_highlightpic pic ON pic.contentId = content.id"
        . " LEFT JOIN mod_extrafieldsJoinContent"
        . " ON mod_extrafieldsJoinContent.contentId = content.id "
       // . " LEFT JOIN mod_extrafields ON mod_extrafields.id = mod_extrafieldsJoinContent.extrafieldsId"
        . " LEFT JOIN joinContentLanguage ON joinContentLanguage.mainContentId = content.id"
        . " LEFT JOIN content AS contentTrans ON joinContentLanguage.translatedContentId = contentTrans.id AND joinContentLanguage.language = '".$language."'"
        . " LEFT JOIN translation AS readMoreTrans ON readMoreTrans.sourceId = content.id AND readMoreTrans.siteId = content.siteId AND readMoreTrans.code = 'contentTxtReadMoreLink' AND readMoreTrans.language = '".$language."'"
        . " LEFT JOIN translation AS readMoreTexteTrans ON readMoreTexteTrans.sourceId = content.id AND readMoreTexteTrans.siteId = content.siteId AND readMoreTexteTrans.language = '".$language."'"
        . " LEFT JOIN translation AS transSubtitle ON transSubtitle.sourceId = content.id AND transSubtitle.siteId = content.siteId AND transSubtitle.code = 'contentSubtitle' AND transSubtitle.language = '".$language."'"
        . " LEFT JOIN translation AS transIntro ON transIntro.sourceId = content.id AND transIntro.siteId = content.siteId AND transIntro.code = 'contentIntroduction' AND transIntro.language = '".$language."'"
        . " WHERE content.isPublished = 1 "
        . $queryFilter . " "
        . $order . " " .$direction." "
        . $limitFilter." ;" ;

        // exit($sql);
        $results = $this->db->fetchAll($sql, $queryParams);

        $rewrited = array();
        
        $siteService = new \App\Vertuoz\Api\Service\SiteService($this->db, $this->appId);
        $siteLanguage = $siteService->get()->getLanguage();

        //Extrafields
        foreach ($results as $result) {

            $toReturn = false;
            if(empty($extraFilters)){
                $toReturn = true;
            }
            
            //Recherche des extrafields dans la config de la zone
            //Exemple : fields##OL##mod-extrafields-3,mod-highlightpic####titleCssClass##TXT##....

            $areaConfig = explode("##",current(explode("####",$result['areaConfig'])));
            $areaConfig = explode(",",$areaConfig[count($areaConfig)-1]); //ici on a la liste des champs en tableau : mod-extrafields-3,mod-highlightpic

            //On ne garde que les extrafields car on a toute la config
            for($i = count($areaConfig) -1 ; $i >= 0 ; $i--) {
                if(preg_match("/^mod\-extrafields\-.+$/", $areaConfig[$i]) == 0)
                    array_splice($areaConfig,$i,1);
            }

            //Récupération de la config globale des extrafields
            $extraFieldsConfig = explode("####",$result["fieldsConfig"]);
            for($i = 0; $i < count($extraFieldsConfig); $i++)
                $extraFieldsConfig[$i] = current(explode("##",$extraFieldsConfig[$i]));

            //Définition des extrafields utilisés pour cette zone de contenu
            $finalExtrafields = [];
            for($i = 0; $i < count($areaConfig); $i++) {
                $finalExtrafields[$extraFieldsConfig[(int)str_replace("mod-extrafields-","",$areaConfig[$i])]] = [$i, (int)str_replace("mod-extrafields-","",$areaConfig[$i])];
            }

            $finalExtrafields2 = [];
            $i2 = 0;
            for($i =0; $i< count($extraFieldsConfig);$i++)
            {
                
                $label = $extraFieldsConfig[$i];
                $positionContent = null;
                $positionConfig = $i;
                
                //Override par le content attendu
                foreach($finalExtrafields as $lib => $data)
                {
                    if($label == $lib)
                    {
                        $positionContent = $i2;
                        $positionConfig = $data[1];
                        $i2++;
                    }
                }
                
                
                $finalExtrafields2[$label] = array($positionContent,$positionConfig);
            }
            
            //Récupération des potentielles valeurs
            foreach($finalExtrafields2 as $label => $positionContentPositionConfig) {
                
                $positionContent = $positionContentPositionConfig[0];
                $positionConfig  = $positionContentPositionConfig[1];
                
                if(is_null($positionContent))
                {
                    $result["extrafields"]["language1"][] = ["config"=>null,"label"=>null,"value"=>  null];
                    continue;
                }
                if (strtoupper($result['language']) == strtoupper($siteLanguage)) {
                    
                    $ifOriginalLanguage = $this->db->fetchAssoc("SELECT mod_extrafield_get_field(".$positionContent.",".$result['id'].") AS content");

                    if(!empty($extraFilters)){
                        if(!$toReturn){
                            if(array_key_exists($positionConfig, $extraFilters) && $extraFilters[$positionConfig] == Encoding::fixUTF8($ifOriginalLanguage['content']) && !is_null($ifOriginalLanguage['content'])){
                                $toReturn = true;
                            }
                        }
                    }
                    $result["extrafields"]["language1"][] = ["config"=>null,"label"=>addslashes(Encoding::fixUTF8($label)),"value"=> Encoding::fixUTF8($ifOriginalLanguage['content'])];
                }
                else {
                    $resultExtraLabelAndContent = $this->db->fetchAssoc(
                        "SELECT t.value AS label, (SELECT t.value AS content FROM translation AS t WHERE t.siteId = ".$result['siteId']." AND t.sourceId = ".$positionConfig." AND t.code = 'modExtrafieldsFieldListValue' AND t.language = '".$result['language']."') AS content "
                        . " FROM translation AS t WHERE t.siteId = ".$result['siteId']
                        . " AND t.sourceId = ".$positionConfig
                        . " AND t.code = 'modExtrafieldsFieldName'"
                        . " AND t.language = '".$result['language']."'");

                    if (is_null($resultExtraLabelAndContent['label']) || strlen($resultExtraLabelAndContent['label']) == 0) {
                        $resultExtraLabelAndContent['label'] = addslashes($label);
                    }
                    
                    $resultExtraContentOrigine = $this->db->fetchAssoc(
                        "SELECT mod_extrafield_get_field(".$positionContent.",".$result['id'].") AS content");

                    if (is_null($resultExtraLabelAndContent['content']) || strlen($resultExtraLabelAndContent['content']) == 0) {
                        
                        $contenuFinal = $resultExtraContentOrigine['content'];
                       
                        // Test si c'est un champ autre que liste et qu'il y a une traduction
                        $labelToSearch = Encoding::fixUTF8(str_replace(" ","-",$label));
                        $resultExtraContent = $this->db->fetchAssoc(
                            "SELECT t.value AS content"
                            . " FROM translation AS t WHERE t.siteId = ".$result['siteId']
                            . " AND t.code LIKE '%".$labelToSearch."%'"
                            . " AND t.sourceId = ".$result['id']
                            . " AND t.language = '".$result['language']."'");

                        if($resultExtraContent){
                            $contenuFinal = $resultExtraContent['content'];
                        }
                    }
                    else {
                        
                        // Récupération de la config du site
                        $resultConfigSite = $this->db->fetchAssoc(
                                "SELECT * FROM argweb.mod_extrafields WHERE siteId =".$result['siteId']
                        );
                        
                        $explodeConfig = explode("####",$resultConfigSite["fieldsLanguage1"]);
                        $explodeConfigFine = explode("##",$explodeConfig[$positionConfig]);
                    
                        if($explodeConfigFine[1] == "L"){
                            $explodeValues = explode(",",$explodeConfigFine[2]);

                            $reformatedEnList = "- empty -,".$resultExtraLabelAndContent['content'];
                            $translatedValueIndex = array_search($resultExtraContentOrigine['content'],$explodeValues);
                   
                            $explode = explode(',', $reformatedEnList);
                            $contenuFinal = trim($explode[$translatedValueIndex]);
                        }
                        
//                        foreach($explodeConfig as $config){
//                            $explodeConfigFine = explode("##",$config);
//                            
//                            if($explodeConfigFine[0] == $label){
//                                $explodeValues = explode(",",$explodeConfigFine[2]);
//                                $translatedValueIndex = array_search($resultExtraContentOrigine['content'],$explodeValues);
//                            }
//                        }
                       
                        // le $positionContent n'est pas bon car il repose sur le système d'extrafield de base, pas sur le système de la table translation
                        
                    }
                    
                    if(!empty($extraFilters)){
                        if(!$toReturn){
                            if(array_key_exists($positionConfig, $extraFilters) && $extraFilters[$positionConfig] == Encoding::fixUTF8($contenuFinal) && !is_null($contenuFinal)){
                                $toReturn = true;
                            }
                        }
                    }

                    $result["extrafields"]["language1"][$positionConfig]["config"] = null;
                    $result["extrafields"]["language1"][$positionConfig]["label"] = $resultExtraLabelAndContent['label'];
                    $result["extrafields"]["language1"][$positionConfig]["value"] = $contenuFinal;
                    
                }
            }
            
            $result["galeries"] = array();
            
            // Récupération des albums photos liés au contenu
            $sqlGaleries = "SELECT * FROM mod_linkmodule mlm LEFT JOIN mod_photosAlbum mpa ON mpa.id = mlm.moduleId WHERE moduleName = 'Photos' AND contentId = ".$result["id"];
            $arrGaleries = $this->db->fetchAll($sqlGaleries, $queryParams);
            
            foreach($arrGaleries as $galerie){
                $result["galeries"][$galerie["moduleId"]] = array();
                $result["galeries"][$galerie["moduleId"]]["id"] = $galerie["id"];
                $result["galeries"][$galerie["moduleId"]]["name"] = $galerie["name"];
                $result["galeries"][$galerie["moduleId"]]["description"] = $galerie["description"];
                $result["galeries"][$galerie["moduleId"]]["showInfosInFullScreen"] = $galerie["showInfosInFullScreen"];
                $result["galeries"][$galerie["moduleId"]]["showInfosInListing"] = $galerie["showInfosInListing"];
                $result["galeries"][$galerie["moduleId"]]["showAlbumInfos"] = $galerie["showAlbumInfos"];
                $result["galeries"][$galerie["moduleId"]]["showInAllAlbumView"] = $galerie["showInAllAlbumView"];
                $result["galeries"][$galerie["moduleId"]]["slideBorderType"] = $galerie["slideBorderType"];
                $result["galeries"][$galerie["moduleId"]]["photos"] = array();
                
                // Récupération des photos de cette galerie
                $sqlPhotos = "SELECT * FROM mod_photosPicture WHERE albumId = ".$galerie["id"];
                $arrPhotos = $this->db->fetchAll($sqlPhotos, $queryParams);
                
                foreach($arrPhotos as $photo){
                    array_push($result["galeries"][$galerie["moduleId"]]["photos"],$photo);
                }
            }
          
            //Suppression du retour des champs areaConfig et fieldsConfig car inutiles
            unset($result["areaConfig"]);
            unset($result["fieldsConfig"]);
            
            // Remplacement des valeurs nulles par des string vide pour faire plaisir à Twig
            foreach($result as $key => $value){
                if(is_null($value)){
                    $result[$key] = "";
                }
            }
            if($toReturn){
                $rewrited[] = $result;
            }
        }
        
        if (!empty($rewrited)) {
            if(!is_null($startValue) && !is_null($limitValue)) {
                $rewrited = array_slice($rewrited, $startValue, $limitValue);
            }
            elseif(!is_null($limitValue)) {
                $rewrited = array_slice($rewrited, 0, $limitValue);
            }
        }

        return $rewrited;
    }

    /*
      function update($id, $dataToUpdate) {
      $result = $this->db->update("users", $dataToUpdate, array("id" => $id));
      return $result;
      }

      function delete($id) {

      $result = $this->db->delete("users", array("id" => (int) $id));
      return $result;
      }

      function create($data) {

      $result = $this->db->insert("users", $data);
      return $result;
  } */
}
