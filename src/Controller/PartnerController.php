<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\PartnerType;
use App\Form\PartnerPasswordType;
use App\Helper\PartnerFormHelper;
use App\Helper\UploadFileHelper;
use App\Decorator\PartnerDecorator;
use App\Helper\UtilsHelper;

class PartnerController extends Controller {

    /**
     * @Route("/partner/account", name="partner_account")
     */
    public function account(Request $request) {

        $username = $this->getUser()->getUsername();
        
        $objPartner = $this->get('partner.service')->loadByUsername($username);
        
        return $this->render('partner/account.html.twig', [
            'page' => 'account',
            'current_nav' => 'account',
            'transactions' => $objPartner->getTransactions()
        ]);
        
    }
    
    /**
     * @Route("/partner/customers", name="partner_customers")
     */
    public function customers(Request $request) {

        $username = $this->getUser()->getUsername();
        
        $objPartner = $this->get('partner.service')->loadByUsername($username);
        
        return $this->render('partner/customers.html.twig', [
            'current_nav' => 'customers',
            'page' => 'account',
            'customers' => $objPartner->getCustomers()
        ]);
        
    }
    
    
    /**
     * @Route("/partner/profile", name="partner_profile")
     */
    public function profile(Request $request) {

        $twigData = [];
        $partnerFormErrors = [];
        $partnerLogoErrors = [];
        
        $username = $this->getUser()->getUsername();
        
        $objPartner = $this->get('partner.service')->loadByUsername($username);
        $countries = $this->get('country.service')->loadAll();
        
        $partnerForm = $this->createForm(PartnerType::class, null, array(
            'partner' => $objPartner,
            'countries' => $countries
        ));
        
        $partnerPasswordForm = $this->createForm(PartnerPasswordType::class, null, array(
            'partner' => $objPartner
        ));
        
        $partnerForm->handleRequest($request);
        $partnerPasswordForm->handleRequest($request);

        if ($partnerForm->isSubmitted() && $partnerForm->isValid()) {
            
            $files = $request->files->all();
            $partnerLogo = $files['partner']['logo'];
            
            $partnerData = $partnerForm->getData();
            $partnerFormErrors = $this->get('partner.form.helper')->getFormPartnerErrors($partnerData);
    
            if (count($partnerFormErrors) == 0) {
                
                if (!is_null($partnerLogo)) {
            
                    $partnerLogoErrors = UploadFileHelper::getErrors($partnerLogo);
                   
                    if (count($partnerLogoErrors) == 0) {
                        $filename = md5(uniqid(rand(), true)).'.'.$partnerLogo->guessExtension();
                        $partnerLogo->move(PartnerDecorator::PHOTOS_PATH, $filename);
                        $uploadedFile = UtilsHelper::resize(200, PartnerDecorator::PHOTOS_PATH.'/'.$filename, PartnerDecorator::PHOTOS_PATH.'/'.$filename, false);
                        if (!is_null($objPartner->Logo)) {
                            @unlink(PartnerDecorator::PHOTOS_PATH.'/'.$objPartner->Logo);
                        }
                        $partnerData['logo'] = $uploadedFile;
                    }
                    
                }
                
                $objPartner = $this->get('partner.service')->save($objPartner, $partnerData);
            }
            
            foreach ($partnerFormErrors as $error) {
                $this->addFlash(
                    'error',
                    $error
                );
            }
            
            foreach ($partnerLogoErrors as $error) {
                $this->addFlash(
                    'error',
                    $error
                );
            }
            
            if (count($partnerFormErrors) + count($partnerLogoErrors) == 0) {
                $this->addFlash(
                    'success',
                    'Modifications saved !'
                );
            }
             
        }
        
        if ($partnerPasswordForm->isSubmitted() && $partnerPasswordForm->isValid()) {
            
            $objUser = $this->getUser();
            $partnerData = $partnerPasswordForm->getData();
            $partnerFormErrors = $this->get('partner.form.helper')->getFormPartnerPasswordErrors($objPartner, $objUser, $partnerData);
          
            if (count($partnerFormErrors) == 0) {
                $objPartner = $this->get('partner.service')->savePassword($objPartner, $objUser, $partnerData);
            }
            
            foreach ($partnerFormErrors as $error) {
                $this->addFlash(
                    'error',
                    $error
                );
            }
            
            if (count($partnerFormErrors) == 0) {
                $this->addFlash(
                    'success',
                    'Modifications saved !'
                );
            }
            
        }
        
        return $this->render('partner/profile.html.twig', [
            'current_nav' => 'profile',
            'partner' => $objPartner,
            'partner_form' => $partnerForm->createView(),
            'partner_password_form' => $partnerPasswordForm->createView()
        ]);
        
    }
//    
//    /**
//     * @Route("/partner/reset-password", name="partner_reset_password")
//     */
//    public function reset(Request $request) {
//
//        return $this->render('partner/reset-password.html.twig');
//    }

}



