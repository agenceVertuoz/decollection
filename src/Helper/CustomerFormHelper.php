<?php

namespace App\Helper;
use App\Helper\UtilsHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CustomerFormHelper {
    
    protected $container;
    
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }
    
    public static function getCountriesChoices($arrCountries) {
        
        $countries = [];
        
        if (!is_null($arrCountries)) {
            foreach ($arrCountries as $objCountry) {
                $countries[$objCountry->Name] = $objCountry->Id;
            }
        }
        
        return $countries;
        
    }
    
    public function getFormCustomerErrors($data) {
        
        $errors = [];
        $blnToReturn = true;
        
        // Vérification de la validité de l'e-mail
        if (strlen($data['email']) > 0 && !UtilsHelper::isValidEmail($data['email'])) {
            $blnToReturn = false;
            $errors[] = "Please enter a valid e-mail address";
        }
        
        return $errors;
        
    }

    
}