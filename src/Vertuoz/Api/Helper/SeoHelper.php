<?php

namespace App\Vertuoz\Api\Helper;

class SeoHelper {

    protected $client;

    public function __construct(\App\Vertuoz\Api\ApiClient $client) {
        $this->client = $client;
    }

    public function get($params) {
        return $this->client->get('/seo/page?' . http_build_query($params));
    }

}
