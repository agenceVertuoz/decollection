<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Form\CustomerType;
use App\Service\CustomerService;
use App\Helper\CustomerFormHelper;

class CustomerController extends Controller {
    
    
    /**
     * @Route("/customer/create", name="customer_create")
     */
    public function create(Request $request) {

        $twigData = [];
        $customerFormErrors = [];

        $countries = $this->get('country.service')->loadAll();

        $customerForm = $this->createForm(CustomerType::class, null, array(
            'mode' => 'create',
            'route' => $this->generateUrl('customer_create'),
            'customer' => null,
            'countries' => $countries
        ));

        return $this->render('customer/edit.html.twig', [
            'customer_form' => $customerForm->createView(),
        ]);
    }
    

    /**
     * @Route("/customer/edit", name="customer_edit")
     */
    public function edit(Request $request) {

        $twigData = [];
        $customerFormErrors = [];

        $customerId = $request->query->get('customerId');

        $objCustomer = $this->get('customer.service')->loadById($customerId);
        if (!$objCustomer) {
            throw new NotFoundHttpException('The customer does not exist');
        }

        $countries = $this->get('country.service')->loadAll();

        $customerForm = $this->createForm(CustomerType::class, null, array(
            'mode' => 'edit',
            'route' => $this->generateUrl('customer_edit'),
            'customer' => $objCustomer,
            'countries' => $countries
        ));

        return $this->render('customer/edit.html.twig', [
            'customer_form' => $customerForm->createView(),
        ]);
    }

    /**
     * @Route("/customer/process", name="customer_process")
     */
    public function process(Request $request) {

        $errors = [];
        $customerData = $request->query->get('customer');

        $customerId = $customerData['customer_id'];
      
        $objPartner = $this->get('partner.service')->loadByUsername($this->getUser()->getUsername());
        
        $objCustomer = null;
        
        if (!is_null($customerId) && !empty($customerId)) {

            $objCustomer = $this->get('customer.service')->loadById($customerId);
            if (!$objCustomer) {
                throw new NotFoundHttpException('The customer does not exist');
            }
        
        }

        $customerFormErrors = CustomerFormHelper::getFormCustomerErrors($customerData);

        if (count($customerFormErrors) == 0) {
            $objCustomer = $this->get('customer.service')->save($objCustomer, $objPartner, $customerData);
        }

        foreach ($customerFormErrors as $error) {
            $this->addFlash(
                'error', $error
            );
        }

        if (count($customerFormErrors) == 0) {
            $this->addFlash(
                'success', 'Modifications saved !'
            );
        }

        return new Response();
    }
    
    
    /**
     * @Route("/customer/delete", name="customer_delete")
     */
    public function delete(Request $request) {

        $twigData = [];
        
        $customerId = $request->query->get('customerId');

        $objCustomer = $this->get('customer.service')->loadById($customerId);
        if (!$objCustomer) {
            throw new NotFoundHttpException('The customer does not exist');
        }
        
        $objCustomer = $this->get('customer.service')->delete($objCustomer);

        return $this->redirectToRoute('partner_customers');
        
    }

}
