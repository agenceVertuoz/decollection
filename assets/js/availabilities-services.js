
module.exports = {
    getAvailabilitiesByPropertyId: getAvailabilitiesByPropertyId
};

var $modal = $('#default-modal');

function getAvailabilitiesByPropertyId(propertyId, year) {
    var jxhr = $.get('availabilities/list?propertyId=' + propertyId + '&year=' + year, function (data) {
        $modal.find('.modal-title').text("Availabilities");
        $modal.find('.modal-body').html(data);
        $modal.modal('show');
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
}


