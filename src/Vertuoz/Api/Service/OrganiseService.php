<?php

namespace App\Vertuoz\Api\Service;

use App\Vertuoz\Api\Model\Organise as OrganiseModel;
use App\Vertuoz\Api\Service\SiteService AS SiteService;

class OrganiseService {

    protected $db;
    protected $appId;
    protected $siteModel;
    protected $organiseModel;

    public function __construct($db, $appId) {
        
        $this->db = $db;
        $this->appId = $appId;
        
        $this->organiseModel = new OrganiseModel($this->db, $this->appId);
        
        $siteService = new SiteService($this->db, $this->appId);
        $this->siteModel = $siteService->get();
        
    }
    
    public function getOne($params)
    {
        if(!isset($params['language']))
            $params['language'] = $this->siteModel->getLanguage();
        else {
            if(!in_array(strtoupper($params['language']), $this->siteModel->getLanguagesAvailables())) {
                throw new \Exception("Langue '".$params['language']."' non autorisée", 400);
            }
            else
                $this->siteModel->setLanguage($params['language']);
        }

        return $this->organiseModel->getOne($params);
    }

    public function getAllByFieldsValue($params) {

        $params = $this->checkFilters($params);
        $results = $this->organiseModel->getAll($params);

        return $results;
    }

    private function checkFilters($params) {

        //Vérififcation champs
        $allowedFields = array("siteId","area", "language");

        foreach ($params as $k => $v) {
            if (!in_array($k, $allowedFields)) {
                throw new \Exception("Filtre $k non autorisé, filtres attendus : " . implode(", ", $allowedFields), 400);
            }
        }

        //Définition de la langue par défaut si non précisé, vérification si ok dans le site si précisée
        if(!isset($params['language']))
            $params['language'] = $this->siteModel->getLanguage();
        else {
            if(!in_array(strtoupper($params['language']), $this->siteModel->getLanguagesAvailables())) {
                throw new \Exception("Langue '".$params['language']."' non autorisée", 400);
            }
            else
                $this->siteModel->setLanguage($params['language']);
        }

        return $params;
    }

}
