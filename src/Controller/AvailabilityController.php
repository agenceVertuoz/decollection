<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Service\AvailabilityService;
use App\Service\TransactionService;
use App\Helper\AvailabilityHelper;
use App\Helper\TransactionHelper;
use App\Helper\CalendarHelper;

class AvailabilityController extends Controller {

    /**
     * @Route("/availabilities/list", name="availabilities_list")
     */
    public function index(Request $request) {

        $twigData = [];

        $propertyId = $request->query->get('propertyId');
        $year = $request->query->get('year', date('Y'));

        $arrAvailabilities = $this->get('availability.service')->loadAllByPropertyId($propertyId);
        $arrTransactions = $this->get('transaction.service')->loadAllByPropertyId($propertyId);


        for ($month = 1; $month <= 12; $month++) {

            $currentYearMonth = $year . '-' . str_pad($month, 2 , '0', STR_PAD_LEFT) . '-01';
            $arrAvailabilitiesPerMonth = AvailabilityHelper::getAvailabilitiesPerMonth($arrAvailabilities, $currentYearMonth);
            $arrTransactionsPerMonth = TransactionHelper::getTransactionsPerMonth($arrTransactions, $currentYearMonth);

            $arrAvailabilitiesCalendar = array_replace($arrAvailabilitiesPerMonth, $arrTransactionsPerMonth);

            $twigData['calendar'][$month] = CalendarHelper::getHtml($arrAvailabilitiesCalendar, $currentYearMonth);
            $twigData['propertyId'] = $propertyId;
            $twigData['year'] = $year;
            
        }

        return $this->render('availabilities/list.html.twig', $twigData);
    }

}



