<?php

namespace App\Vertuoz\Api\Decorator;

use App\Vertuoz\Decorator\AbstractDecorator;

class SlideshowPictureDecorator extends AbstractDecorator implements \JsonSerializable {

    const COMPONENT_CLASS = '\ModSlideshowPicture';

    public function __construct($component) {

        parent::__construct($component);
        
    }
    
    public function jsonSerialize()
    {
        return [
            'name' => $this->Name,
            'file' => $this->File,
            'link' => $this->Link,
            'textlink' => $this->Textlink,
            'targetBlankLink' => $this->TargetBlankLink
        ];
    }

}
