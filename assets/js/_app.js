
require('../scss/app.scss');

const $ = require('jquery');
const Popper = require('popper.js');
const Bootstrap = require('bootstrap');
const Modernizr = require('../lib/modernizr.js');
const Easing = require('jquery.easing');
const Headroom = require('headroom.js');
const Swiper = require('swiper/dist/js/swiper');
const ScrollReveal = require('scrollreveal/dist/scrollreveal');
const Leaflet = require('leaflet/dist/leaflet.js');
const Datepicker = require('bootstrap-datepicker');
const Selectpicker = require('bootstrap-select');

window.$ = window.jQuery = require("jquery");

global.Headroom = Headroom;
global.Swiper = Swiper;
global.ScrollReveal = ScrollReveal;
global.Leaflet = Leaflet;