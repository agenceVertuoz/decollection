<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Service\PropertyService;
use App\Service\PartnerService;
use App\Form\TransactionType;

class PropertyController extends Controller
{
    /**
     * @Route("/{slug}-{id}", name="property", requirements={"slug"=".*", "id"="\d+"})
     */
    public function indexAction(Request $request, $slug, $id)
    {
        
        $objProperty = $this->get('property.service')->loadById($id);
        if (!$objProperty) {
            throw new NotFoundHttpException('The property does not exist');
        }
        
        $objPartner = $this->get('partner.service')->loadByUsername($this->getUser()->getUsername());
        
        $transactionForm = $this->createForm(TransactionType::class, null, array(
            'property' => $objProperty,
            'partner' => $objPartner,
        ));
        
        return $this->render('property/index.html.twig', [
            'page' => 'property',
            'property' => $objProperty,
            'transaction_form' => $transactionForm->createView()
        ]);
        
    }
    
    
    /**
     * @Route("/property/list", name="property_list")
     */
    public function listAction()
    {
        
        $arrProperties = $this->get('property.service')->loadAll();
 
        return $this->render('property/list.html.twig', [
            'page' => 'property-list',
            'properties' => $arrProperties
        ]);
        
    }
    
}
