<?php

namespace App\Vertuoz\Api\Model;
class Organise {

    function __construct($db, $appId) {
        $this->db = $db;
        $this->appId = $appId;
    }

    function getOne($params) {
        
        $params['siteId'] = $this->appId;

        $res = $this->getAll($params);
        if (is_array($res) && count($res) > 0) {
            return $res[0];
        } else {
            return null;
        }
    }

    function getAll($params) {
        
        $params['siteId'] = $this->appId;

        $queryFilter = null;
        $queryParams = array();
        $language = $params['language'];
        unset($params['language']);

        foreach ($params as $name => $value) {
            if (!is_null($value)) {
                $queryFilter .= " AND mod_organiseVocabulary.$name = ?";
                $queryParams[] = $value;
            }
        }

        $sql = "SELECT"
        . " COALESCE(t.value, mod_organiseVocabulary.name) AS name,"
        . " mod_organiseVocabulary.alias,"
        . " mod_organiseVocabulary.aliasLanguage2,"
        . " mod_organiseVocabulary.aliasLanguage3,"
        . " mod_organiseVocabulary.area,"
        . " mod_organiseVocabulary.displayChildrenContentOnParent,"
        . " mod_organiseVocabulary.id,"
        . " mod_organiseVocabulary.nameLanguage2,"
        . " mod_organiseVocabulary.nameLanguage3,"
        . " mod_organiseVocabulary.siteId"
        . " FROM mod_organiseVocabulary "
        . " LEFT JOIN translation AS t ON t.sourceId = mod_organiseVocabulary.id AND t.code = 'modOrganiseVocabularyName' AND t.language = '" . $language . "'"
        . " WHERE 1 = 1 "
        . $queryFilter . " "
        . " LIMIT 0,1000 ";

        $results = $this->db->fetchAll($sql, $queryParams);

        // Menus
        foreach ($results as $key => $result) {
            $result["menus"] = $this->getMenus($result["id"], $language) ;
            $results[$key] = $result ;
        }

        return $results;
    }

    function getMenus($vocabularyId, $language) {


        // Nouvelle requète avec traduction
        $sql = " SELECT "
                ." menu.id,"
                ." menu.vocabularyId,"
                ." menu.parentId,"
                ." menu.picture,"
                ." COALESCE(transName.value,menu.name) AS name,"
                ." menu.nameLanguage2,"
                ." menu.nameLanguage3,"
                ." COALESCE(transAlias.value,menu.alias) AS alias,"
                ." menu.aliasLanguage2,"
                ." menu.aliasLanguage3,"
                ." menu.enableAutoChildrenAreaname,"
                ." menu.enableAutoChildrenTerm,"
                ." menu.enableAutoChildrenShopCategoryId,"
                ." menu.position"
                ." FROM mod_organiseTerm AS menu"
                ." LEFT JOIN translation AS transName ON menu.id = transName.sourceId AND transName.code = 'modOrganiseTermName' AND transName.language = '".$language."'"
                ." LEFT JOIN translation AS transAlias ON menu.id = transAlias.sourceId AND transAlias.code = 'modOrganiseTermAlias' AND transAlias.language = '".$language."'"
                ." WHERE menu.vocabularyId = ".$vocabularyId.""
                ." AND menu.parentId IS NULL "
                ." ORDER BY menu.position;" ;

        $menus = $this->db->fetchAll($sql);

        foreach ($menus as $key => $menu) {

            $sql = "SELECT * FROM mod_organiseTerm"
                . " WHERE vocabularyId = ".$vocabularyId.""
                . " AND parentId = ".$menu["id"]." "
                . " ORDER BY position ;" ;

            $menu["submenus"] = $this->db->fetchAll($sql);

            $menus[$key] = $menu ;
        }

        return $menus ;
    }

    /*
      function update($id, $dataToUpdate) {
      $result = $this->db->update("users", $dataToUpdate, array("id" => $id));
      return $result;
      }

      function delete($id) {

      $result = $this->db->delete("users", array("id" => (int) $id));
      return $result;
      }

      function create($data) {

      $result = $this->db->insert("users", $data);
      return $result;
      } */
}
