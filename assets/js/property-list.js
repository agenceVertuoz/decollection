
//require('../scss/app.scss');

const jQueryLazy = require('jquery-lazy');



// Navbar - Headroom.js
// --------------------

var nav = document.querySelector("nav");
var headroom = new Headroom(nav);
headroom.init();



// Images lazy load
// --------------------

var $imgsToPreload = [];
var imgsPreloaded = 0;

$.each($('[data-src]'), function () {
    $imgsToPreload.push($(this));
});

$("[data-src]").Lazy({
    threshold: 100000,
    afterLoad: function(element) {
        imgsPreloaded++;
        var progressW = $('.progress').width();
        var progress = progressW * (imgsPreloaded / $imgsToPreload.length);
        $('.progress .progress-bar').width(progress);
        
    },
    onFinishedAll: function() {
       setTimeout(function () {
            hideLoader();
            setTimeout(function () {
                bindAnimations();
            }, 300);
        }, 300);
    }
     
 });
         


function hideLoader() {
    $('.loader').fadeOut();
    $('body').removeClass('noscroll');
}

function bindAnimations() {

    // ScrollReveal
    // --------------------

    ScrollReveal().reveal('.reveal', {delay: 500, duration: 800 });
//
//    ScrollReveal().reveal('.reveal', {delay: 1000, duration: 1200});
//    ScrollReveal().reveal('.reveal-property-overview-1', {delay: 0, duration: 600});
//    ScrollReveal().reveal('.reveal-property-overview-2', {delay: 600, duration: 600});

}