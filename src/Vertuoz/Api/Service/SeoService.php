<?php

namespace App\Vertuoz\Api\Service;

use App\Vertuoz\Api\Model\Page as PageModel;
use App\Vertuoz\Api\Service\SiteService AS SiteService;

class SeoService {

    protected $db;
    protected $appId;
    
    protected $siteModel;
    protected $pageModel;

    public function __construct($db, $appId) {
        
        $this->db = $db;
        $this->appId = $appId;
        
        $siteService = new SiteService($this->db, $this->appId);
        $this->siteModel = $siteService->get();
     
        $this->pageModel = new PageModel($this->appId);
        
    }
    
    public function get($uri, $locale) {
        
        $pagesArray = $this->pageModel->loadAll();
        $languageId = $this->siteModel->getLanguageId($locale);
        
        $pageFound = null;
        $pageFoundUrl = null;
        $pageTitle = null;
        $pageDescription = null;
        $pageRobots = "index,follow";
        
        if ($languageId == 1) {
            $pageTitle = $this->siteModel->name;
            $pageDescription = $this->siteModel->metaDescription;
        }
        if ($languageId == 2) {
            $pageTitle = $this->siteModel->nameLanguage2;
            $pageDescription = $this->siteModel->metaDescriptionLanguage2;
        }
        if ($languageId == 3) {
            $pageTitle = $this->siteModel->nameLanguage3;
            $pageDescription = $this->siteModel->metaDescriptionLanguage3;
        }
        if ($languageId == 4) {
            $pageTitle = $this->siteModel->nameLanguage4;
            $pageDescription = $this->siteModel->metaDescriptionLanguage4;
        }
        if ($languageId == 5) {
            $pageTitle = $this->siteModel->nameLanguage5;
            $pageDescription = $this->siteModel->metaDescriptionLanguage5;
        }
        
        foreach ($pagesArray as $page) {
            
            $pageUrl = urldecode($page->Url);

            if (strstr($pageUrl, '*') === FALSE && urldecode($uri) == $pageUrl && is_null($pageFound)) {
                $pageFound = $page;
                $pageFoundUrl = $pageUrl;
                break;
            } else {
                $pageUrl = str_replace('*', '', $pageUrl);
                if ($pageUrl == substr(urldecode($uri), 0, strlen($pageUrl)) && is_null($pageFound)) {
                    $pageFound = $page;
                    $pageFoundUrl = $pageUrl;
                    break;
                }
            }
        }
        
        if (!is_null($pageFound)) {

            $pageConfig = $pageFound->getConfig();
            
            if (isset($pageConfig['title ' . strtoupper($locale)]) && is_string(stristr($pageConfig['title ' . strtoupper($locale)], "DEFAULT"))) {
                $pageTitle = str_replace("DEFAULT", $pageTitle, $pageConfig['title ' . strtoupper($locale)]);
            } else if (isset($pageConfig['title ' . strtoupper($locale)]) && strlen($pageConfig['title ' . strtoupper($locale)]) > 0)
                $pageTitle = $pageConfig['title ' . strtoupper($locale)];

            if (isset($pageConfig['description ' . strtoupper($locale)]) && is_string(stristr($pageConfig['description ' . strtoupper($locale)], "DEFAULT"))) {
                $pageDescription = str_replace("DEFAULT", $pageDescription, $pageConfig['description ' . strtoupper($locale)]);
            } else if (isset($pageConfig['description ' . strtoupper($locale)]) && strlen($pageConfig['description ' . strtoupper($locale)]) > 0)
                $pageDescription = $pageConfig['description ' . strtoupper($locale)];

            $pageRobots = str_replace(" ", ",", $pageConfig['bots']);

            //Bloquer accès
            if ($pageConfig['blocked'] == "true")
                \QApplication::Redirect("error.php?code=maintenance");

        }
        
        return [
            'url' => $pageFoundUrl,
            'title' => $pageTitle,
            'description' => $pageDescription,
            'robots' => $pageRobots
        ];
        
    }

}
