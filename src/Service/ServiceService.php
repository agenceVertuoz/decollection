<?php

namespace App\Service;
use App\Decorator\ServiceDecorator;

class ServiceService {

    public function loadServiceByPropertyId($serviceId, $propertyId) {
        
        $objService = \ModSpecific30JoinPropertyService::QuerySingle(
            \QQ::AndCondition(
                \QQ::Equal(\QQN::ModSpecific30JoinPropertyService()->PropertyId, $propertyId),
                \QQ::Equal(\QQN::ModSpecific30JoinPropertyService()->ServiceId, $serviceId)
            )
        );
      
        $objDecoratedService = self::decorate($objService);

        return $objDecoratedService;
        
    }
    
    
    public static function decorate($objService) {

        $objDecoratedService = new ServiceDecorator($objService);

        return $objDecoratedService;
        
    }

}
