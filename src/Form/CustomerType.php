<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Helper\CustomerFormHelper;

class CustomerType extends AbstractType
{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $mode = $options['mode'];
        $route = $options['route'];
        $customer = $options['customer'];
        $countriesChoices = CustomerFormHelper::getCountriesChoices($options['countries']);
        
        switch ($mode) {
            
            case 'create':
                
                $customer_id = null;
                $firstname = null;
                $lastname = null;
                $email = null;
                $company = null;
                $address = null;
                $postalCode = null;
                $city = null;
                $state = null;
                $country_id = null;
                $homePhone = null;
                $officePhone = null;
                $homeCellular = null;
                $officeCellular = null;
                $homeFax = null;
                $officeFax = null;
            break;
        
            case 'edit':
                
                $customer_id = $customer->getId();
                $firstname = $customer->getFirstname();
                $lastname = $customer->getLastname();
                $email = $customer->getEmail();
                $company = $customer->getCompany();
                $address = $customer->getAddress();
                $address = $customer->getAddress();
                $postalCode = $customer->getPostalCode();
                $city = $customer->getCity();
                $state = $customer->getState();
                $country_id = $customer->getCountryId();
                $homePhone = $customer->getHomePhone();
                $officePhone = $customer->getOfficePhone();
                $homeCellular = $customer->getHomeCellular();
                $officeCellular = $customer->getOfficeCellular();
                $homeFax = $customer->getHomeFax();
                $officeFax = $customer->getOfficeFax();
            break;
        
        }
       
        $builder
                
            ->setAction($route)
                
            ->add('customer_id', HiddenType::class, array(
                'required' => true,
                'attr' => array('autocomplete' => 'off'),
                'data' => $customer_id
            ))
                
            ->add('firstname', TextType::class, array(
                'required' => true,
                'attr' => array('autocomplete' => 'off'),
                'data' => $firstname
            ))
                
            ->add('lastname', TextType::class, array(
                'required' => true,
                'attr' => array('autocomplete' => 'off'),
                'data' => $lastname
            ))
                
            ->add('email', TextType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $email
            ))
                
            ->add('company', TextType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $company
            )) 
                
            ->add('address', TextareaType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $address
            )) 
                
            ->add('zipcode', TextType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $postalCode
            )) 
                
            ->add('city', TextType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $city
            )) 
                
            ->add('state', TextType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $state
            )) 
                
            ->add('country', ChoiceType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'choices' => $countriesChoices,
                'data' => $country_id
            )) 
                
            ->add('home_phone', TextType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $homePhone
            )) 
                
            ->add('office_phone', TextType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $officePhone
            )) 
                
            ->add('home_cellular', TextType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $homeCellular
            )) 
                
            ->add('office_cellular', TextType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $officeCellular
            ))
                
            ->add('home_fax', TextType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $homeFax
            )) 
                
            ->add('office_fax', TextType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $officeFax
            )) 
                
            ->add('save', SubmitType::class, array('label' => 'Save'))
        
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('mode');
        $resolver->setRequired('route');
        $resolver->setRequired('customer');
        $resolver->setRequired('countries');
    }
    
}

