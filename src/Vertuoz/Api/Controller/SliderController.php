<?php

namespace App\Vertuoz\Api\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\DBAL\Driver\Connection;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpFoundation\Request;
use App\Vertuoz\Api\Service\SliderService;
use App\Vertuoz\Api\Helper\SliderHelper;
//use Symfony\Component\Serializer\SerializerInterface;

class SliderController extends Controller
{
    /**
     * @Route("/slider", name="api_slider", methods={"GET"})
     */
    public function slider(Request $request, Connection $connection)
    {

        $baseParams = array();

        $params = $request->query->all();

        $fullParams = array_merge($baseParams, $params);

        $sliderService = new SliderService($connection, $this->getParameter('app.id'));

        $results = $sliderService->getOne($fullParams);
        
        return $this->json($results);
        
//        $instanceId = $request->query->get('instanceId');
//        if (!$instanceId) {
//            throw new BadRequestHttpException('Instance id is missing');
//        }
//        
//        $objSlideshow = $this->get('api.slider.service')->loadById($instanceId);
//        if (!$objSlideshow) {
//            throw new NotFoundHttpException('The slideshow does not exist');
//        }
//        
//        $arrPictures = $this->get('api.slider.service')->getPictures($instanceId);
//
//        return $this->json([
//            'slider' => $objSlideshow,
//            'pictures' => $arrPictures
//        ]);
        
    }
}
