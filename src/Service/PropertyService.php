<?php

namespace App\Service;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\Decorator\PropertyDecorator;

class PropertyService {

    const PUBLISH_MODE = 6;
    
    protected $container;
    protected $objPartner;
    
    public function __construct(ContainerInterface $container) {
        
        $this->container = $container;
        
        $username = $this->container->get('security.token_storage')->getToken()->getUser()->getUsername();
        $this->objPartner = $this->container->get('partner.service')->loadByUsername($username);
        
    }

    public function loadAll() {
        
        $objConditions = [];
        
        $arrIds = array() ;
        $arrAllowed = \ModSpecific30AllowedProperty::LoadArrayByPartnerId($this->objPartner->Id);
        
        foreach($arrAllowed as $objAllowed) {
            array_push($arrIds , $objAllowed->PropertyId);
        }
        
        array_push($objConditions, \QQ::In(\QQN::ModSpecific30Property()->Id, $arrIds));
        array_push($objConditions, \QQ::Equal(\QQN::ModSpecific30Property()->PublishMode, self::PUBLISH_MODE)); 
        array_push($objConditions, \QQ::Equal(\QQN::ModSpecific30Property()->IsActivated, 1));
        array_push($objConditions, \QQ::Equal(\QQN::ModSpecific30Property()->IsCompleted, 1));
        array_push($objConditions, \QQ::Equal(\QQN::ModSpecific30Property()->IsDeleted, 0));
        
        $arrProperties = \ModSpecific30Property::QueryArray(
            \QQ::AndCondition($objConditions),
            \QQ::Clause(
                \QQ::Expand(
                    \QQ::Virtual("photos", \QQ::SubSql("SELECT GROUP_CONCAT(file ORDER BY isMainPicture DESC, position ASC) FROM mod_specific30Photos WHERE propertyId = {1} GROUP BY propertyId", \QQN::ModSpecific30Property()->Id))
                ),
                \QQ::OrderBy(\QQN::ModSpecific30Property()->HomePagePosition, true)
            )
        );
        
        $arrDecoratedProperties = self::decorateCollection($arrProperties);

        return $arrDecoratedProperties;
        
    }
    
    
    public function loadById(int $id) {
        
        $objAllowed = \ModSpecific30AllowedProperty::QuerySingle(
            \QQ::AndCondition(
                \QQ::Equal(\QQN::ModSpecific30AllowedProperty()->PropertyId , $id),
                \QQ::Equal(\QQN::ModSpecific30AllowedProperty()->PartnerId , $this->objPartner->Id)
            )
        );
        
        if (is_null($objAllowed)) {
            return null;
        }
     
        $objProperty = \ModSpecific30Property::QuerySingle(
            \QQ::AndCondition(
                \QQ::Equal(\QQN::ModSpecific30Property()->Id, $id), 
                \QQ::Equal(\QQN::ModSpecific30Property()->PublishMode, self::PUBLISH_MODE), 
                \QQ::Equal(\QQN::ModSpecific30Property()->IsActivated, 1), 
                \QQ::Equal(\QQN::ModSpecific30Property()->IsCompleted, 1)
            ),
            \QQ::Clause(
                \QQ::Expand(
                    \QQ::Virtual("photos", \QQ::SubSql("SELECT GROUP_CONCAT(file ORDER BY isMainPicture DESC, position ASC) FROM mod_specific30Photos WHERE propertyId = {1} GROUP BY propertyId", \QQN::ModSpecific30Property()->Id))
                )
            )
        );
        
        $objJoinPropertyService = \ModSpecific30JoinPropertyService::QueryArray(
            \QQ::Equal(\QQN::ModSpecific30JoinPropertyService()->PropertyId, $objProperty->Id)
        );
        
        $objServiceCategory = \ModSpecific30ServiceCategory::LoadAll();
        
        $objJoinPropertyAmenity = \ModSpecific30JoinAmenityProperty::QueryArray(
            \QQ::Equal(\QQN::ModSpecific30JoinAmenityProperty()->PropertyId, $objProperty->Id)
        );
        
        $objAmenityCategory = \ModSpecific30AmenityCategory::LoadAll();
      
        $objDecoratedProperty = self::decorate($objProperty, $objJoinPropertyService, $objServiceCategory, $objJoinPropertyAmenity, $objAmenityCategory);
        
        return $objDecoratedProperty;
        
    }
    

    
    public static function decorateCollection(array $arrProperties) {

        $arrDecoratedProperties = [];
        
        foreach ($arrProperties as $objProperty) {
            $objNewProperty = self::decorate($objProperty);
            $arrDecoratedProperties[] = $objNewProperty;
        }
        
        return $arrDecoratedProperties;
        
    }
    
    public static function decorate($objProperty, $objPropertyServices = null, $objJoinServiceCategory = null, $objAmenityCategory = null, $objJoinPropertyAmenity = null) {

        $objDecoratedProperty = new PropertyDecorator($objProperty, $objPropertyServices, $objJoinServiceCategory, $objAmenityCategory, $objJoinPropertyAmenity);

        return $objDecoratedProperty;
        
    }

}
