<?php

namespace App\Vertuoz\Api\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Doctrine\DBAL\Driver\Connection;
use Symfony\Component\HttpFoundation\Request;
use App\Vertuoz\Api\Service\SeoService;

class SeoController extends AbstractController
{
    /**
     * @Route("/seo/page", name="seo_page", methods={"GET"})
     */
    public function seoPage(Request $request, Connection $connection)
    {
        
        $appId = $this->getParameter('app.id');
        $locale = $request->getLocale();

        $uri = $request->query->get('uri');
        $language = $request->query->get('language');
        
        if (!$uri) {
            throw new BadRequestHttpException("Uri is missing");
        }
        
        if ($language) {
           $locale = $language; 
        }
        
        $seoService = new SeoService($connection, $appId);

        $results = $seoService->get($uri, $locale);
        
        return $this->json($results);
        
    }
}
