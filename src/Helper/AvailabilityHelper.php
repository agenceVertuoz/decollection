<?php

namespace App\Helper;

class AvailabilityHelper {


    public static function getAvailabilitiesPerMonth($arrAvailabilites, $currentYearMonth) {

        $arrAvailabilitiesPerMonth = [];
        $currentYearMonth = explode("-", $currentYearMonth);
        $ndDayCurrentMonth = intval(date("t", mktime(0, 0, 0, $currentYearMonth[1], 1, $currentYearMonth[0])));

        foreach ($arrAvailabilites as $objAvailability) {

            $startDate = $objAvailability->StartDate->PHPDate("Y-m-d");
            $startDateTab = explode('-', $startDate);

            $startYear = (int) $startDateTab[0];
            $startMonth = $startDateTab[1];
            $startDay = (int) $startDateTab[2];

            $endMonth = $objAvailability->EndDate->PHPDate("Y-m-d");
            $endDateTab = explode('-', $endMonth);

            $endYear = (int) $endDateTab[0];
            $endMonth = (int) $endDateTab[1];
            $endDay = (int) $endDateTab[2];


            // La réservation commence avant le mois et se termine apres ou pendant
            if (date("Y-m-d", mktime(0, 0, 0, $startMonth, $startDay, $startYear)) < date("Y-m-d", mktime(0, 0, 0, $currentYearMonth[1], 1, $currentYearMonth[0])) && date("Y-m-d", mktime(0, 0, 0, $endMonth, $endDay, $endYear)) >= date("Y-m-d", mktime(0, 0, 0, $currentYearMonth[1], 1, $currentYearMonth[0]))) {

                //Elle se termine après, je remplis mon mois en entier
                if (date("Y-m-d", mktime(0, 0, 0, $endMonth, $endDay, $endYear)) > date("Y-m-d", mktime(0, 0, 0, $currentYearMonth[1], $ndDayCurrentMonth, $currentYearMonth[0]))) {
                    for ($i = 1; $i <= $ndDayCurrentMonth; $i++)
                        $arrAvailabilitiesPerMonth[$i - 1] = $objAvailability->Type;
                }
                //Elle se termine pendant, je remplis jusqu'au jour de fin
                else {
                    for ($i = 1; $i <= $endDay; $i++)
                        $arrAvailabilitiesPerMonth[$i - 1] = $objAvailability->Type;
                }
            }

            // La réservation commence pendant le mois
            if (date("Y-m-d", mktime(0, 0, 0, $startMonth, $startDay, $startYear)) >= date("Y-m-d", mktime(0, 0, 0, $currentYearMonth[1], 1, $currentYearMonth[0])) && date("Y-m-d", mktime(0, 0, 0, $startMonth, $startDay, $startYear)) <= date("Y-m-d", mktime(0, 0, 0, $currentYearMonth[1], $ndDayCurrentMonth, $currentYearMonth[0]))) {

                //Elle se termine apres, je remplis mon mois en entier à partir du jour de début
                if (date("Y-m-d", mktime(0, 0, 0, $endMonth, $endDay, $endYear)) > date("Y-m-d", mktime(0, 0, 0, $currentYearMonth[1], $ndDayCurrentMonth, $currentYearMonth[0]))) {
                    for ($i = $startDay; $i <= $ndDayCurrentMonth; $i++)
                        $arrAvailabilitiesPerMonth[$i - 1] = $objAvailability->Type;
                }
                // Elle se termine pendant, je remplis de mon jour de début jusqu'au jour de fin
                else {
                    for ($i = $startDay; $i <= $endDay; $i++)
                        $arrAvailabilitiesPerMonth[$i - 1] = $objAvailability->Type;
                }
            }
        }

        return $arrAvailabilitiesPerMonth;
    }

}
