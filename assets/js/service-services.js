
module.exports = {
    show: show,
};

var $modal = $('#default-modal');

function show(serviceId, propertyId) {
    var jxhr = $.get('service/show?serviceId=' + serviceId + '&propertyId=' + propertyId, function (data) {
        $modal.find('.modal-title').text("Service");
        $modal.find('.modal-body').html(data);
        $modal.modal('show');
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
}


