<?php

namespace App\Vertuoz\Helper;

class UriHelper {
    
    public static function slugify(string $string) {
        return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string), '-'));
    }
    
}