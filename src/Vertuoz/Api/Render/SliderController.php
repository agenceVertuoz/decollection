<?php

namespace App\Vertuoz\Api\Render;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Vertuoz\Api\Helper\SliderHelper;

class SliderController extends Controller
{
    /**
     * @Route("/render-slider", name="render_slider", methods={"GET"})
     */
    public function renderSlider(Request $request)
    {
        $apiClient = $this->get('api.client');
        $twigData = array();

        $tplName = $request->query->get('tplName', 'default');
        $slideSize = $request->query->get('slideSize', '10000x10000');
        $name = $request->query->get('name');
        
        if (!$name) {
            throw new BadRequestHttpException("Name is missing");
        }
        
        $params['name'] = $name;

        $baseViewPath = "api/slider";
        $viewPath = $baseViewPath . "/" . $tplName;
   
        try {
            $sliderHelper = new SliderHelper($apiClient);
            $slider = $sliderHelper->get($params);
        } catch (\GuzzleHttp\Exception\ClientException $e) { }
        
        if (empty($slider)) {
            $viewPath = "api/error/notfound.html.twig";
            return $this->render($viewPath, array("message" => sprintf("Slider <strong>%s</strong> non trouvé", $params['name'])));
        }
        else {
            foreach ($slider['slides'] as $key => $slide) {
                $imgUrl = SliderHelper::getSlidePictureFullUrl($this->getParameter('app.cdn.url'), $this->getParameter('app.id'), $slideSize, $slide['file']);
                $slider['slides'][$key]['file'] = $imgUrl;
            }
        }
        
        $twigData["slider"] = $slider;
      
        return $this->render($viewPath . '.html.twig', $twigData);
    }
}
