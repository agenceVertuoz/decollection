<?php

namespace App\Vertuoz\Api\Helper;

class SliderHelper {

    protected $client;

    public function __construct(\App\Vertuoz\Api\ApiClient $client) {
        $this->client = $client;
    }

    public function get($params) {
        return $this->client->get('/slider?' . http_build_query($params));
    }
    
    public static function getSlidePictureFullUrl($baseUrl, $appId, $sizes, $path) {
        return $baseUrl . '/documents/' . $appId . '/slideshow3/' . $sizes . '_' . $path;
    }

}
