<?php

namespace App\Vertuoz\Api\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\DBAL\Driver\Connection;
use Symfony\Component\HttpFoundation\Request;
use App\Vertuoz\Api\Service\ContentService;

class ContentController extends AbstractController
{
    /**
     * @Route("/contents", name="contents", methods={"GET"})
     */
    public function contents(Request $request, Connection $connection)
    {
        
        $baseParams = array();

        $params = $request->query->all();

        $fullParams = array_merge($baseParams, $params);
        
        $fullParams["siteId"] = $this->getParameter('app.id');

        $contentService = new ContentService($connection, $this->getParameter('app.id'));

        $results = $contentService->getAllByFieldsValue($fullParams);
        
        return $this->json($results);
        
    }
}
