<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Helper\UtilsHelper;
use App\Helper\PartnerHelper;
use App\Security\User\Partner;


class SecurityController extends Controller {

    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils) {
        
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'page' => 'login',
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }
    
    /**
     * @Route("/reset_password", name="reset_password")
     */
    public function reset(Request $request) {

        $error = null;
        $lastEmail = null;
        
        $key = $request->query->get('key');
        $email = $request->query->get('email');
        
        $_email = $request->request->get('_email');
        $partner = $request->request->get('partner_password');
        
        if (!is_null($key)) {
            
            $objPartner = $this->get('partner.service')::loadByEmail($email);
            if (!$objPartner) {
                throw new NotFoundHttpException('The partner does not exist');
            }
            
            $appKey = $this->getParameter('app.key');
            if (!PartnerHelper::isValidKey($objPartner, $key, $appKey)) {
                throw new AccessDeniedHttpException('Access denied');
            }
            
        }

        if (is_null($key) && !is_null($_email)) {
            
            if (!UtilsHelper::isValidEmail($_email)) {
                
                $this->addFlash(
                    'error',
                    'E-mail address invalid.'
                );
                
                return $this->redirectToRoute('reset_password');
                
            }
        
            $objPartner = $this->get('partner.service')::loadByEmail($_email);

            if (is_null($objPartner)) {
                
                $this->addFlash(
                    'error',
                    'E-mail address not found.'
                );
                
                return $this->redirectToRoute('reset_password');
                
            }
            
            $appKey = $this->getParameter('app.key');
            $url = $this->generateUrl('reset_password', array('email' => $_email, 'key' => PartnerHelper::generateKey($objPartner, $appKey)), UrlGeneratorInterface::ABSOLUTE_URL);
            
            if (!$this->get('mailer.service')->sendForgotPassword($objPartner, $url)) {
                $this->addFlash(
                    'error',
                    'An issue has been encoutered. Please retry later.'
                );
            }
            else {
                $this->addFlash(
                    'success',
                    'An e-mail has been sent to you. Please click on the link in the e-mail to reset your password.'
                );
            }
            
            return $this->redirectToRoute('reset_password');
        
        }
        
        if (!is_null($key) && !is_null($email) && !is_null($partner) && is_null($_email)) {
            
            $partnerData['new_password'] = $partner['new_password'];
            $partnerData['new_password_confirm']= $partner['new_password_confirm'];
            
            $objUser = new Partner($objPartner->getLogin(), '', '', ['ROLE_AGENT']);
            
            $partnerFormErrors = $this->get('partner.form.helper')->getFormPartnerPasswordErrors($objPartner, $objUser, $partnerData);
          
            if (count($partnerFormErrors) === 0) {
                $objPartner = $this->get('partner.service')->savePassword($objPartner, $objUser, $partnerData);
            }
            
            foreach ($partnerFormErrors as $error) {
                $this->addFlash(
                    'error',
                    $error
                );
            }
            
            if (count($partnerFormErrors) === 0) {
                $this->addFlash(
                    'success',
                    'Modifications saved !'
                );
            }
            
            //return $this->redirectToRoute('reset_password');
            
        }
        
        return $this->render('security/reset-password.html.twig', [
            'current_nav' => '',
            'last_email' => $lastEmail,
            'error' => $error,
        ]);
        
    }

}
