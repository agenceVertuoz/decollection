<?php

namespace App\Vertuoz\Api\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\DBAL\Driver\Connection;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpFoundation\Request;
use App\Vertuoz\Api\Service\OrganiseService;
use Symfony\Component\Serializer\SerializerInterface;

class OrganiseController extends Controller
{
    /**
     * @Route("/organise", name="api_organise", methods={"GET"})
     */
    public function organise(Request $request, Connection $connection)
    {
  
        $baseParams = array();

        $params = $request->query->all();

        $fullParams = array_merge($baseParams, $params);

        $organiseService = new OrganiseService($connection, $this->getParameter('app.id'));

        $results = $organiseService->getOne($fullParams);

        return $this->json($results);
    
    }
        
}
