<?php

namespace App\Vertuoz\Api\Render;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpFoundation\Request;
use App\Vertuoz\Api\Helper\OrganiseHelper;

class OrganiseController extends Controller
{
    /**
     * @Route("/render-organise", name="render_organise", methods={"GET"})
     */
    public function renderOrganise(Request $request)
    {
        $apiClient = $this->get('api.client');
        $twigData = array();

        $tplName = $request->query->get('tplName', 'default');
        $params = $request->query->get('params');
        
        if (!$params['area']) {
            throw new BadRequestHttpException("Area is missing");
        }
     
        $baseViewPath = "api/organise";
        $viewPath = $baseViewPath . "/" . $tplName;
  
        try {
            $organiseHelper = new OrganiseHelper($apiClient);
            $organise = $organiseHelper->get($params);
            
        } catch (\GuzzleHttp\Exception\ClientException $e) {}
  
        if (empty($organise)) {
            $viewPath = "api/error/notfound.html.twig";
            return $this->render($viewPath, array("message" => sprintf("Organise zone <strong>%s</strong> non trouvé", $params['area'])));
        }
        
        $twigData["organise"] = $organise;
      
        return $this->render($viewPath . '.html.twig', $twigData);
    }
}
