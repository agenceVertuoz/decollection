<?php

namespace App\Decorator;

use App\Vertuoz\Decorator\AbstractDecorator;
use App\Vertuoz\Helper\EncodingHelper;

class ServiceDecorator extends AbstractDecorator {

    const COMPONENT_CLASS = '\ModSpecific30JoinPropertyService';
    const PHOTOS_PATH = 'http://rootcms.luxury-rentals.com/documents/30/specific30agents/services';

    public function __construct($component) {

        parent::__construct($component);
    }

    public function getService() {

        if (!is_null($this->ServiceId)) {
            return $this->ServiceIdObject;
        }

        return null;
    }
    
public function getName() {

        $name = null;

        if (!is_null($this->ServiceId)) {
            return $this->ServiceIdObject->Name;
        }

        return $name;
    }

    public function getDescription() {

        if (!is_null($this->Description)) {
            return html_entity_decode(EncodingHelper::fixUTF8($this->Description));
        } else {
            if (!is_null($this->ServiceId) && !is_null($this->ServiceIdObject->Description)) {
                return html_entity_decode(EncodingHelper::fixUTF8($this->ServiceIdObject->Description));
            }
        }

        return null;
    }
    
    public function getImage() {
//        
//        if (!is_null($this->Image)) {
//            return self::PHOTOS_PATH . '/' . $this->Image;
//        } else {
            if (!is_null($this->ServiceId) && !is_null($this->ServiceIdObject->Image)) {
                return self::PHOTOS_PATH . '/' . $this->ServiceIdObject->Image;
            }
//        }

        return null;
        
    }

}
