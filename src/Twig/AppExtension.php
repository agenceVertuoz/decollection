<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return array(
            new TwigFilter('addslashes', array($this, 'addslashes')),
        );
    }

    public function addslashes($string)
    {
        return addslashes($string);
    }
}
