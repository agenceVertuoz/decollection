<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Helper\PartnerFormHelper;

class PartnerType extends AbstractType
{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $partner = $options['partner'];
        $countriesChoices = PartnerFormHelper::getCountriesChoices($options['countries']);
       
        $builder
                
            ->add('logo', FileType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off')
            )) 
                
            ->add('firstname', TextType::class, array(
                'required' => true,
                'attr' => array('autocomplete' => 'off'),
                'data' => $partner->getFirstname()
            ))
                
            ->add('lastname', TextType::class, array(
                'required' => true,
                'attr' => array('autocomplete' => 'off'),
                'data' => $partner->getLastname()
            ))
                
            ->add('email', TextType::class, array(
                'required' => true,
                'attr' => array('autocomplete' => 'off', 'readonly' => true),
                'data' => $partner->getEmail()
            ))
                
            ->add('company', TextType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $partner->getCompany()
            )) 
                
            ->add('address', TextareaType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $partner->getAddress()
            )) 
                
            ->add('zipcode', TextType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $partner->getPostalCode()
            )) 
                
            ->add('city', TextType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $partner->getCity()
            )) 
                
            ->add('state', TextType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $partner->getState()
            )) 
                
            ->add('country', ChoiceType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'choices' => $countriesChoices,
                'data' => $partner->getCountryId()
            )) 
                
            ->add('home_phone', TextType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $partner->getHomePhone()
            )) 
                
            ->add('office_phone', TextType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $partner->getOfficePhone()
            )) 
                
            ->add('home_cellular', TextType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $partner->getHomeCellular()
            )) 
                
            ->add('office_cellular', TextType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $partner->getOfficeCellular()
            ))
                
            ->add('home_fax', TextType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $partner->getHomeFax()
            )) 
                
            ->add('office_fax', TextType::class, array(
                'required' => false,
                'attr' => array('autocomplete' => 'off'),
                'data' => $partner->getOfficeFax()
            )) 
                
            ->add('save', SubmitType::class, array('label' => 'Save'))
        
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('partner');
        $resolver->setRequired('countries');
    }
    
}

