<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use App\Decorator\TransactionDecorator;
use App\Helper\TransactionHelper;

class TransactionService {
    
    protected $container;
    
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }
    
    public function loadById($transactionId) {
        
        $username = $this->container->get('security.token_storage')->getToken()->getUser()->getUsername();
        $objPartner = $this->container->get('partner.service')->loadByUsername($username);
      
        $objTransaction = \ModSpecific30Transactions::QuerySingle(
            \QQ::AndCondition(
                \QQ::Equal(\QQN::ModSpecific30Transactions()->Id, $transactionId),
                \QQ::Equal(\QQN::ModSpecific30Transactions()->PartnerId, $objPartner->getId()),
                \QQ::Equal(\QQN::ModSpecific30Transactions()->PropertyIdObject->IsActivated, 1),
                \QQ::Equal(\QQN::ModSpecific30Transactions()->PropertyIdObject->IsCompleted, 1),
                \QQ::Equal(\QQN::ModSpecific30Transactions()->PropertyIdObject->IsDeleted, 0)
            )
        );
        $objDecoratedTransaction = self::decorate($objTransaction);

        return $objDecoratedTransaction;
        
    }

    public function loadAllByPropertyId($propertyId) {
        
        $arrTransactions = \ModSpecific30Transactions::QueryArray(
            \QQ::AndCondition(
                    \QQ::Equal(\QQN::ModSpecific30Transactions()->PropertyIdObject->Id, $propertyId),
                    \QQ::GreaterThan(\QQN::ModSpecific30Transactions()->Status, 3),
                    \QQ::NotEqual(\QQN::ModSpecific30Transactions()->Status, 6)
            ),
            \QQ::Clause(
                \QQ::OrderBy(
                    \QQN::ModSpecific30Transactions()->DateArrival, true
                )
            )
        );
        
        $arrDecoratedTransactions = self::decorateCollection($arrTransactions);

        return $arrDecoratedTransactions;
        
    }
    
    public static function decorateCollection(array $arrTransactions) {

        $arrDecoratedTransactions = [];
        
        foreach ($arrTransactions as $objTransaction) {
            $objNewTransaction = self::decorate($objTransaction);
            $arrDecoratedTransactions[] = $objNewTransaction;
        }
        
        return $arrDecoratedTransactions;
        
    }
    
    public static function decorate($objTransaction) {

        $objDecoratedTransaction = new TransactionDecorator($objTransaction);

        return $objDecoratedTransaction;
        
    }
    
    public function updateStatus($objProperty, $objTransaction, $objPartner) {
        
        $status = null;
        $bookDate = null;
        
        switch ($objTransaction->Status) {
            
            case 2:
                $status = 4;
                $lblStatus = "Hold";
            break;
        
            case 5:
                $status = 8;
                $lblStatus = "Book";
                $bookDate = \QDateTime::Now();
            break;
        
        }
        
        if (!is_null($status)) {
            
            $objTransaction->Status             = $status;
            $objTransaction->ApprovedDeniedDate = \QDateTime::FromTimestamp(time() + TransactionHelper::$holdDuration);
            $objTransaction->Comment            = TransactionHelper::formatComment($objTransaction->Comment, $objPartner, $status);
            $objTransaction->BookDate           = $bookDate;
            $objTransaction->Save();
            
            $propertyUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]" . $objProperty->getUri();
            $siteUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
            $phpMailer = $this->container->get('mailer.service')->getMailer();
            $email = $this->container->getParameter('app.email');
            
            \ModSpecific30Transactions::sendTransactionEmailNotificationToAdmin2($phpMailer, $email, $propertyUrl, $objTransaction, TransactionHelper::$lblStatus[$objTransaction->Status]);
            \ModSpecific30Transactions::sendTransactionEmailNotificationToPartner2($phpMailer, $email, $propertyUrl, $siteUrl, $objTransaction, TransactionHelper::$lblStatus[$objTransaction->Status]);
        }
        
        return $objTransaction;
        
    }
    
    public function save($objProperty, $objPartner, $objCustomer, $type, $arrivalDate, $departureDate, $nbAdults, $nbChildren, $comment) {
      
        $objTransaction = \ModSpecific30Transactions::QuerySingle(
            \QQ::AndCondition(
                \QQ::NotEqual(\QQN::ModSpecific30Transactions()->Status, 0),
                \QQ::Equal(\QQN::ModSpecific30Transactions()->PropertyId, $objProperty->Id), 
                \QQ::Equal(\QQN::ModSpecific30Transactions()->PropertyIdObject->IsActivated, 1), 
                \QQ::Equal(\QQN::ModSpecific30Transactions()->PropertyIdObject->IsCompleted, 1), 
                \QQ::Equal(\QQN::ModSpecific30Transactions()->PropertyIdObject->IsDeleted, 0), 
                \QQ::Equal(\QQN::ModSpecific30Transactions()->DateArrival, $arrivalDate), 
                \QQ::Equal(\QQN::ModSpecific30Transactions()->DateDeparture, $departureDate),
                \QQ::Equal(\QQN::ModSpecific30Transactions()->PartnerId, $objPartner->Id),
                \QQ::Equal(\QQN::ModSpecific30Transactions()->CustomerId, $objCustomer->Id)
            )
        );
        
        $status = TransactionHelper::getStatusByType($type);
        
        if (is_null($objTransaction)) {
            
            $netRental = TransactionHelper::getNetRental($objProperty, $arrivalDate, $departureDate);
            $commission = TransactionHelper::getCommission($netRental, $objPartner->getPercentCommission());
            $netOwner = TransactionHelper::getNetOwner($objProperty, $arrivalDate, $departureDate);
            $netExternal = TransactionHelper::getNetExternal($objProperty, $arrivalDate, $departureDate);
            $costRental = TransactionHelper::getCostRental($netRental, $commission);
     
            $objTransaction = new \ModSpecific30Transactions();
            $objTransaction->PartnerId = $objPartner->Id;
            $objTransaction->CustomerId = $objCustomer->Id;
            $objTransaction->PropertyId = $objProperty->Id;
            $objTransaction->NbAdults = $nbAdults;
            $objTransaction->NbChildren = $nbChildren;
            $objTransaction->SecurityDeposit = $objProperty->SecurityDeposit;
            $objTransaction->DateArrival = \QDateTime::FromTimestamp(strtotime($arrivalDate));
            $objTransaction->DateDeparture = \QDateTime::FromTimestamp(strtotime($departureDate));
            $objTransaction->Status = $status;
            $objTransaction->NetRental = $netRental;
            $objTransaction->ValidatedNetRental = 0;
            $objTransaction->NetOwner = $netOwner;
            $objTransaction->NetExternal = $netExternal;
            $objTransaction->Commission = $commission;
            $objTransaction->CostRental = $costRental;
            $objTransaction->DepositDue = $objProperty->getDeposit();
            $objTransaction->Comment = TransactionHelper::formatComment($comment, $objPartner, $status);
            $objTransaction->ApprovedDeniedDate = TransactionHelper::getApprovedDeniedDate();
            $objTransaction->DateCreation = \QDateTime::FromTimestamp(time());
            $objTransaction->Save();
            
            TransactionHelper::saveInfos($objTransaction, $arrivalDate, $departureDate);
                
        }
        else {
            
            $objTransaction->Status = $status;
            $objTransaction->Comment = TransactionHelper::formatComment($comment, $objPartner, $status);
            $objTransaction->Save();
                
        }
        
        $email = $this->container->getParameter('app.email');
        $propertyUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]" . $objProperty->getUri();
        $phpMailer = $this->container->get('mailer.service')->getMailer();
        //die(var_dump($objTransaction->PropertyIdObject->Id));
        \ModSpecific30Transactions::sendTransactionEmailNotificationToAdmin2($phpMailer, $email, $propertyUrl, $objTransaction, TransactionHelper::$lblStatus[$status]);
        
        $notif = new \LuxuryNotifications();
        
        $notif->initialize("notifHoldInfo", 
            array($objProperty->getFullname()), 
            array($objProperty->getFullname())
        );
        
        $phpMailer = $this->container->get('mailer.service')->getMailer();
        
        $notif->sendNotificationPHPMailer($phpMailer, array("info@luxury-rentals.com"));
        
        return true;
        
    }

}
