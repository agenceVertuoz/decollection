<?php

namespace App\Vertuoz\Api\Render;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class SeoController extends Controller
{
    /**
     * @Route("/render-seo", name="render_seo", methods={"GET"})
     */
    public function renderSeo(Request $request)
    {
        $apiClient = $this->get('api.client');
        $twigData = array();
  
        $params = $request->query->get('params');
        
        if (!$params['uri']) {
            throw new BadRequestHttpException("Uri is missing");
        }
        
        $baseParams = [
            'uri' => '',
            'locale' => $request->getLocale()
        ];
        
        $fullParams = array_merge($baseParams, [
            'uri' => $params['uri'],
            'locale' => isset($params['locale']) ? $params['locale'] : null
        ]);
        
        $tplName = $request->query->get('tplName', 'default');


        $baseViewPath = "api/seo";
        $viewPath = $baseViewPath . "/" . $tplName;
   
        try {
            $seoHelper = new \App\Vertuoz\Api\Helper\SeoHelper($apiClient);
            $seo = $seoHelper->get($params);
        } catch (\GuzzleHttp\Exception\ClientException $e) { }
        
        $twigData["seo"] = $seo;
      
        return $this->render($viewPath . '.html.twig', $twigData);
    }
}
