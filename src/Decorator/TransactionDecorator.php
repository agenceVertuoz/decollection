<?php

namespace App\Decorator;

use App\Vertuoz\Decorator\AbstractDecorator;

class TransactionDecorator extends AbstractDecorator {

    const COMPONENT_CLASS = '\ModSpecific30Transactions';
    const RENTAL_CONDITIONS_PATH  = "/var/www/luxuryElocms/modules/30_specific30agents/rentalConditions.html";
    
    public function __construct($component) {

        parent::__construct($component);
 
    }
    
    
    
}
