<?php

namespace App\Vertuoz\Api;

class ApiContext {

    protected static $currentSiteId;
    //const SITE_ID = 30;
    protected static $currentSiteConfig;
    protected static $currentUser;
    protected static $tokenData;

    public static function init($tokenData, $appId) {
        self::$tokenData = $tokenData;
        self::$currentSiteId =$tokenData["site_id"];
    }

    public static function getTokenData() {
        return self::$tokenData;
    }

    public static function getCurrentSiteId() {
        return self::$currentSiteId;
    }

    public static function getCurrentSiteConfig() {
        return self::$currentSiteConfig;
    }

}
