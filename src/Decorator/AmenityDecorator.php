<?php

namespace App\Decorator;

use App\Vertuoz\Decorator\AbstractDecorator;

class AmenityDecorator extends AbstractDecorator {

    const COMPONENT_CLASS = '\ModSpecific30JoinAmenityProperty';

    public function __construct($component) {

        parent::__construct($component);
    }

    public function getAmenity() {

        if (!is_null($this->AmenityId)) {
            return $this->AmenityIdObject;
        }

        return null;
    }
    
public function getName() {

        if (!is_null($this->AmenityId)) {
            return $this->AmenityIdObject->Name;
        }

        return null;
    }

    public function getDescription() {

        if (!is_null($this->Description)) {
            return $this->Description;
        } else {
            if (!is_null($this->AmenityId) && !is_null($this->AmenityIdObject->Description)) {
                return $this->AmenityIdObject->Description;
            }
        }

        return null;
    }
    
    public function getIcon() {
        
//        if (!is_null($this->AmenityId) && !is_null($this->AmenityIdObject->CategoryId) && !is_null($this->AmenityIdObject->Category->Icon)) {
//            return 'icon-' . $this->AmenityIdObject->Category->Icon;
//        }
        
        if (!is_null($this->AmenityIdObject->Icon)) {
            return 'icon-' . $this->AmenityIdObject->Icon;
        }
            
        return null;
        
    }

}
