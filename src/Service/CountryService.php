<?php

namespace App\Service;

class CountryService {
    
    public function loadAll() {
        
        return \ModSpecific30Country::LoadAll(\QQ::Clause(\QQ::OrderBy(\QQN::ModSpecific30Country()->Name)));
        
    }
    
}
