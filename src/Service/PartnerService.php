<?php

namespace App\Service;
use App\Decorator\PartnerDecorator;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PartnerService {
    
    protected $container;
    
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }
    
    public function loadByEmail($email) {
        
        $objDecoratedPartner = null;
        
        $objPartner = \ModSpecific30Partner::QuerySingle(
                \QQ::AndCondition(
                    \QQ::Equal(\QQN::ModSpecific30Partner()->AddressIdObject->Type, 'partner'),
                    \QQ::Equal(\QQN::ModSpecific30Partner()->AddressIdObject->AutomaticDefaultEmail, $email),
                    \QQ::Equal(\QQN::ModSpecific30Partner()->AccesDC, true),
                    \QQ::Equal(\QQN::ModSpecific30Partner()->IsActivated, true)
                )
        );
        
        if ($objPartner) {
            $objDecoratedPartner = self::decorate($objPartner);
        }
  
        return $objDecoratedPartner;
        
    }
    
    public function loadByUsername($username) {
        
        $objPartner = \ModSpecific30Partner::QuerySingle(
            \QQ::AndCondition(
                \QQ::Equal(\QQN::ModSpecific30Partner()->Login, $username),
                \QQ::Equal(\QQN::ModSpecific30Partner()->AccesDC, true),
                \QQ::Equal(\QQN::ModSpecific30Partner()->IsActivated, true)
            )
        );
        
        $arrCustomers = \ModSpecific30Customer::QueryArray(
            \QQ::AndCondition(
                \QQ::Equal(\QQN::ModSpecific30Customer()->IsActivated, 1),
                \QQ::Equal(\QQN::ModSpecific30Customer()->PartnerId, $objPartner->Id)
            ),
            \QQ::Clause(
                \QQ::OrderBy(
                    \QQN::ModSpecific30Customer()->AddressIdObject->Lastname, true
                )
            )
       );
        
        $arrTransactions = \ModSpecific30Transactions::QueryArray(
            \QQ::AndCondition(
                \QQ::Equal(\QQN::ModSpecific30Transactions()->PartnerId, $objPartner->Id),
                \QQ::NotEqual(\QQN::ModSpecific30Transactions()->Status, 0),
                \QQ::Equal(\QQN::ModSpecific30Transactions()->PropertyIdObject->IsActivated, 1),
                \QQ::Equal(\QQN::ModSpecific30Transactions()->PropertyIdObject->IsCompleted, 1),
                \QQ::Equal(\QQN::ModSpecific30Transactions()->PropertyIdObject->IsDeleted, 0)
            ),
            \QQ::Clause(
               \QQ::OrderBy(
                   \QQN::ModSpecific30Transactions()->DateCreation, false
                   //\QQN::ModSpecific30Transactions()->LimitInfo(10)
               )
           )
        );

        $objDecoratedPartner = self::decorate($objPartner, $arrCustomers, $arrTransactions);
  
        return $objDecoratedPartner;
        
    }
    
    public static function decorate($objPartner, $arrCustomers = null, $arrTransactions = null) {

        $objDecoratedPartner = new PartnerDecorator($objPartner, $arrCustomers, $arrTransactions);

        return $objDecoratedPartner;
        
    }
    
    public function updateLastConnection($objPartner) {
        $objPartner->DateLastConnection = \QDateTime::FromTimestamp(time());
        $objPartner->Save();       
    }
    
    public function save($objPartner, $data) {
        
        $objAddress = \ModSpecific30Address::Load($objPartner->AddressId);
        
        $objAddress->Firstname = $data['firstname'];
        $objAddress->Lastname = $data['lastname'];
        $objAddress->Company = $data['company'];
        $objAddress->HomePhone = $data['home_phone'];
        $objAddress->OfficePhone = $data['office_phone'];
        $objAddress->HomeCellular = $data['home_cellular'];
        $objAddress->OfficeCellular = $data['office_cellular'];
        $objAddress->HomeFax = $data['home_fax'];
        $objAddress->OfficeFax = $data['office_fax'];
        $objAddress->Address = $data['address'];
        $objAddress->PostalCode = $data['zipcode'];
        $objAddress->City = $data['city'];
        $objAddress->CountryId = $data['country'];
        $objAddress->State = $data['state'];
        $objAddress->Save();
        
        if (strlen($data['logo']) > 0) {
            $objPartner->Logo = basename($data['logo']);
            $objPartner->Save();
        }
        
        return $objPartner;
        
    }
    
    public function savePassword($objPartner, $objUser, $data) {
        
        $passwordEncoder = $this->container->get("security.password_encoder");
        $password = $passwordEncoder->encodePassword($objUser, $data['new_password']);
       
        $objPartner->Password = $password;
        $objPartner->Save();
        
        return $objPartner;
        
    }
    
}
