<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Helper\TransactionFormHelper;

class TransactionType extends AbstractType
{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $property = $options['property'];
        $partner = $options['partner'];
       
        $customerChoices = TransactionFormHelper::getCustomersChoices($partner->getCustomers());
        
        $builder
                
            ->add('customer', ChoiceType::class, array(
                'required' => true,
                'choices' => $customerChoices,
                'attr' => array('autocomplete' => 'off')
            ))
                
            ->add('arrival_date', TextType::class, array(
                'required' => true,
                'attr' => array('autocomplete' => 'off')
            ))
                
            ->add('departure_date', TextType::class, array(
                'required' => true,
                'attr' => array('autocomplete' => 'off')
            ))
                
            ->add('nb_adults', IntegerType::class, array(
                'required' => true,
                'attr' => array('autocomplete' => 'off', 'min' => 0)
            ))
                
            ->add('nb_children', IntegerType::class, array(
                'required' => true,
                'attr' => array('autocomplete' => 'off', 'min' => 0)
            ))
                
            ->add('property_id', HiddenType::class, array(
                'data' => $property->getId()
            ))    
                
            ->add('type', HiddenType::class, array(
                'data' => ''
            ))
        
            ->add('save', SubmitType::class, array('label' => 'Book now', 'attr' => array('class' => 'd-none')))
        
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('property');
        $resolver->setRequired('partner');
    }
    
}

