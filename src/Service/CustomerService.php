<?php

namespace App\Service;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\Decorator\CustomerDecorator;

class CustomerService {
    
    protected $container;
    
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }
    
    public function loadById($id) {
        
        $username = $this->container->get('security.token_storage')->getToken()->getUser()->getUsername();
        $objPartner = $this->container->get('partner.service')->loadByUsername($username);
        
        $objCustomer = \ModSpecific30Customer::QuerySingle(
            \QQ::AndCondition(
                \QQ::Equal(\QQN::ModSpecific30Customer()->Id, $id),
                \QQ::Equal(\QQN::ModSpecific30Customer()->PartnerId, $objPartner->Id),
                \QQ::Equal(\QQN::ModSpecific30Customer()->IsActivated, true)
            )
        );

        $objDecoratedCustomer = self::decorate($objCustomer);
  
        return $objDecoratedCustomer;
        
    }
    
    public static function decorate($objCustomer) {

        $objDecoratedCustomer = new CustomerDecorator($objCustomer);

        return $objDecoratedCustomer;
        
    }
    
    public function save($objCustomer, $objPartner, $data) {
     
        if (is_null($objCustomer)) {
            $objAddress = new \ModSpecific30Address();
            $objAddress->Type = 'customer';
        }
        else {
            $objAddress = \ModSpecific30Address::Load($objCustomer->AddressId);
        }
        
        
        $objAddress->Firstname = $data['firstname'];
        $objAddress->Lastname = $data['lastname'];
        $objAddress->Company = $data['company'];
        $objAddress->AutomaticDefaultEmail = $data['email'];
        $objAddress->HomePhone = $data['home_phone'];
        $objAddress->OfficePhone = $data['office_phone'];
        $objAddress->HomeCellular = $data['home_cellular'];
        $objAddress->OfficeCellular = $data['office_cellular'];
        $objAddress->HomeFax = $data['home_fax'];
        $objAddress->OfficeFax = $data['office_fax'];
        $objAddress->Address = $data['address'];
        $objAddress->PostalCode = $data['zipcode'];
        $objAddress->City = $data['city'];
        $objAddress->CountryId = $data['country'];
        $objAddress->State = $data['state'];
        $objAddress->Save();
        
        if (is_null($objCustomer)) {
            
            $objCustomer = new \ModSpecific30Customer();
            $objCustomer->PartnerId = $objPartner->getId();
            $objCustomer->AddressId = $objAddress->Id;
            $objCustomer->CurrencyId = null;
            $objCustomer->IsActivated = 1;
            $objCustomer->DateCreation = \QDateTime::fromTimestamp(time());
            $objCustomer->Save();
            
            $objAddress->SourceId = $objCustomer->Id;
            $objAddress->Save();
            
        }
        
        return $objCustomer;
        
    }
    
    public function delete($objCustomer) {
        
        $objCustomer->IsActivated = 0;
        $objCustomer->Save();
        
        return $objCustomer;
        
    }
    
}
