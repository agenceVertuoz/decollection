<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use App\Service\AvailabilityService;
use App\Service\TransactionService;
use App\Service\PartnerService;
use App\Service\CustomerService;
use App\Service\PropertyService;
use App\Helper\AvailabilityHelper;
use App\Helper\TransactionHelper;

class TransactionController extends BaseTransactionController {

    /**
     * @Route("/transaction/update", name="transaction_update")
     */
    public function update(Request $request) {

        $transactionId = $request->query->get('transactionId');
        $twigData = [];
        $twigData['errors'] = [];

        $objTransaction = $this->get('transaction.service')->loadById($transactionId);
        if (!$objTransaction) {
            throw new NotFoundHttpException('The transaction does not exist');
        }
        
        $objProperty = $this->get('property.service')->loadById($objTransaction->PropertyId);
        if (!$objProperty) {
            throw new NotFoundHttpException('The property does not exist');
        }

        $objPartner = $this->get('partner.service')->loadByUsername($this->getUser()->getUsername());

        if ($objTransaction->Status == 2 || $objTransaction->Status == 5) {
            $objTransaction = $this->get('transaction.service')->updateStatus($objProperty, $objTransaction, $objPartner);
        }

        $this->addFlash(
            'success', 'Modifications saved !'
        );

        return new Response();
    }

    /**
     * @Route("/transaction/details", name="transaction_details")
     */
    public function details(Request $request) {

        $transactionId = $request->query->get('transactionId');
        $twigData = [];
        $twigData['errors'] = [];

        $objTransaction = $this->get('transaction.service')->loadById($transactionId);
        if (!$objTransaction) {
            throw new NotFoundHttpException('The transaction does not exist');
        }

        $objProperty = $this->get('property.service')->loadById($objTransaction->PropertyId);
        $objPartner = $this->get('partner.service')->loadByUsername($this->getUser()->getUsername());

        $arrivalDate = $objTransaction->DateArrival->PHPDate('Y-m-d');
        $departureDate = $objTransaction->DateDeparture->PHPDate('Y-m-d');

        $netRental = TransactionHelper::getNetRental($objProperty, $arrivalDate, $departureDate);
        $commission = TransactionHelper::getCommission($netRental, $objPartner->getPercentCommission());
        $netOwner = TransactionHelper::getNetOwner($objProperty, $arrivalDate, $departureDate);
        $netExternal = TransactionHelper::getNetExternal($objProperty, $arrivalDate, $departureDate);
        $costRental = TransactionHelper::getCostRental($netRental, $commission);
        $matchedPrices = TransactionHelper::getMatchedPrices($objProperty, $arrivalDate, $departureDate);

        $rentalConditions = file_get_contents(TransactionHelper::RENTAL_CONDITIONS_PATH);

        return $this->render('transaction/details.html.twig', [
                    'errors' => [],
                    'property' => $objProperty,
                    'customer' => $objTransaction->CustomerIdObject,
                    'arrivalDate' => $objTransaction->DateArrival,
                    'departureDate' => $objTransaction->DateDeparture,
                    'nbAdults' => $objTransaction->NbAdults,
                    'nbChildren' => $objTransaction->NbChildren,
                    'netRental' => $netRental,
                    'commission' => $commission,
                    'costRental' => $costRental,
                    'matchedPrices' => $matchedPrices,
                    'comment' => $objTransaction->Comment,
                    'rentalConditions' => $rentalConditions
        ]);
    }

    /**
     * @Route("/transaction/pricing", name="transaction_pricing")
     */
    public function pricing(Request $request) {
        parent::init();
        return $this->render('transaction/pricing.html.twig', $this->twigData);
    }

    /**
     * @Route("/transaction/byrequest", name="transaction_byrequest")
     */
    public function byrequest(Request $request) {

        parent::init();

        $form = $this->createFormBuilder()
                ->add('comment', TextareaType::class)
                ->add('property_id', HiddenType::class, array(
                    'required' => true,
                    'data' => $this->objProperty->getId()
                ))
                ->add('customer_id', HiddenType::class, array(
                    'required' => true,
                    'data' => $this->objCustomer->getId()
                ))
                ->add('type', HiddenType::class, array(
                    'required' => true,
                    'data' => 2
                ))
                ->add('arrival_date', HiddenType::class, array(
                    'required' => true,
                    'attr' => array('autocomplete' => 'off'),
                    'data' => $this->arrivalDate
                ))
                ->add('departure_date', HiddenType::class, array(
                    'required' => true,
                    'attr' => array('autocomplete' => 'off'),
                    'data' => $this->departureDate
                ))
                ->add('nb_adults', HiddenType::class, array(
                    'required' => true,
                    'attr' => array('autocomplete' => 'off'),
                    'data' => $this->nbAdults
                ))
                ->add('nb_children', HiddenType::class, array(
                    'required' => true,
                    'attr' => array('autocomplete' => 'off'),
                    'data' => $this->nbChildren
                ))
                ->add('save', SubmitType::class, array('label' => 'Save Transaction'))
                ->getForm();

        $this->twigData['byrequest_form'] = $form->createView();

        return $this->render('transaction/byrequest.html.twig', $this->twigData);
    }

    /**
     * @Route("/transaction/hold", name="transaction_hold")
     */
    public function hold(Request $request) {

        parent::init();

        $form = $this->createFormBuilder()
                ->add('comment', TextareaType::class)
                ->add('property_id', HiddenType::class, array(
                    'required' => true,
                    'data' => $this->objProperty->getId()
                ))
                ->add('customer_id', HiddenType::class, array(
                    'required' => true,
                    'data' => $this->objCustomer->getId()
                ))
                ->add('type', HiddenType::class, array(
                    'required' => true,
                    'data' => 3
                ))
                ->add('arrival_date', HiddenType::class, array(
                    'required' => true,
                    'attr' => array('autocomplete' => 'off'),
                    'data' => $this->arrivalDate
                ))
                ->add('departure_date', HiddenType::class, array(
                    'required' => true,
                    'attr' => array('autocomplete' => 'off'),
                    'data' => $this->departureDate
                ))
                ->add('nb_adults', HiddenType::class, array(
                    'required' => true,
                    'attr' => array('autocomplete' => 'off'),
                    'data' => $this->nbAdults
                ))
                ->add('nb_children', HiddenType::class, array(
                    'required' => true,
                    'attr' => array('autocomplete' => 'off'),
                    'data' => $this->nbChildren
                ))
                ->add('save', SubmitType::class, array('label' => 'Save Transaction'))
                ->getForm();

        $this->twigData['hold_form'] = $form->createView();

        return $this->render('transaction/hold.html.twig', $this->twigData);
    }

    /**
     * @Route("/transaction/process", name="transaction_process")
     */
    public function process(Request $request) {

        $errors = [];
        $query = $request->query->get('form');
        $year = $request->query->get('year', date('Y'));

        $objProperty = $this->get('property.service')->loadById($query['property_id']);
        if (!$objProperty) {
            throw new NotFoundHttpException('The property does not exist');
        }

        $objPartner = $this->get('partner.service')->loadByUsername($this->get('security.token_storage')->getToken()->getUser()->getUsername());
        $objCustomer = $this->get('customer.service')->loadById($query['customer_id']);

        $arrAvailabilities = $this->get('availability.service')->loadAllByPropertyId($query['property_id']);
        $arrTransactions = $this->get('transaction.service')->loadAllByPropertyId($query['property_id']);

        $isValid = TransactionHelper::isValidTransaction($arrAvailabilities, $arrTransactions, $query['arrival_date'], $query['departure_date'], $query['type']);

        if (!$isValid) {
            $errors[] = "Unable to create a new reservation on this property for this period.";
        }

        $this->twigData['errors'] = $errors;

        if (count($errors) == 0) {

            $this->get('transaction.service')->save($objProperty, $objPartner, $objCustomer, $query['type'], $query['arrival_date'], $query['departure_date'], $query['nb_adults'], $query['nb_children'], $query['comment']);

            return new Response();
        }

        return $this->render('transaction/pricing.html.twig', $this->twigData);
    }

}
