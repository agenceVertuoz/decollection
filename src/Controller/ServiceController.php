<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ServiceController extends Controller {

    /**
     * @Route("/service/show", name="service_show")
     */
    public function show(Request $request) {

        $twigData = [];

        $serviceId = $request->query->get('serviceId');
        $propertyId = $request->query->get('propertyId');
        
        $objJoinServiceProperty = $this->get('service.service')->loadServiceByPropertyId($serviceId, $propertyId);
        if (!$objJoinServiceProperty) {
            throw new NotFoundHttpException('The service does not exist');
        }
        
        return $this->render('service/show.html.twig', [
            'name' => $objJoinServiceProperty->getName(),
            'description' => $objJoinServiceProperty->getDescription(),
            'image' => $objJoinServiceProperty->getImage()
        ]);
        
    }

}



