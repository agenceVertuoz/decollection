
const TransactionServices = require ('./transaction-services');
const CustomerServices    = require ('./customer-services');

global.TransactionServices = TransactionServices;
global.CustomerServices = CustomerServices;

$('.table [data-transaction-id]').on('click', function(e) {
    
    e.preventDefault();
    var transactionId = $(this).data('transaction-id');
    var params        = 'transactionId=' + transactionId;
    var type          = $(this).data('type');
    
    if (type == "overview") {
        TransactionServices.show(type, params);
    }
    if (type == "update") {
        TransactionServices.update(type, params);
    }
    
});

$('.table [data-customer-id]').on('click', function(e) {
    
    e.preventDefault();
    var customerId  = $(this).data('customer-id');
    var type        = $(this).data('type');
    
    if (type == "edit") {
        CustomerServices.edit(customerId);
    }
    if (type == "delete") {
        CustomerServices.remove(customerId);
    }
    
});

$('a[data-customer-create]').on('click', function(e) {
    
    e.preventDefault();
    CustomerServices.create();
    
});