<?php

namespace App\Helper;

class TransactionFormHelper {
    
    
    public static function getCustomersChoices($arrCustomers) {
        
        $customers = [];
        
        if (!is_null($arrCustomers)) {
            foreach ($arrCustomers as $objCustomer) {
                $customers[$objCustomer['name']] = $objCustomer['id'];
            }
        }
        
        return $customers;
        
    }
        
    public static function getErrors($objProperty, $customerId, $arrivalDate, $departureDate, $nbAdults, $nbChildren) {
        
        $errors = [];
        $blnToReturn = true;
        
        // Vérification de la validité du format de la date d'arrivée
        if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $arrivalDate) == 0) {
            $blnToReturn = false;
            $errors[] = "Please enter a valid Arrival Date (YYYY-mm-dd)";
        }

        // Vérification de la validité du format de la date de départ
        if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $departureDate) == 0) {
            $blnToReturn = false;
            $errors[] = "Please enter a valid Departure Date (YYYY-mm-dd)";
        }

        // Vérification pour les propriétés dont la réservation doit se faire du Samedi au Samedi
        if ($objProperty->getSaturdayToSaturday() && (date('N', strtotime($arrivalDate)) != 6 || date('N', strtotime($departureDate)) != 6)) {
            $blnToReturn = false;
            $errors[] = "Please choose an Arrival Date and a Departure Date on Saturday";
        }

        // Vérification de la date d'arrivée qui doit être supérieure à la date de départ
        if ($arrivalDate > $departureDate) {
            $blnToReturn = false;
            $errors[] = "Departure date must be greater than Arrival date";
        }

        // Vérification de la durée de réservation minimum
        if ($blnToReturn && !is_null($objProperty->getMinWeeks())) {
            $datediff = strtotime($departureDate) - strtotime($arrivalDate);
            $nbDays = floor($datediff/(60*60*24));
            if ($nbDays < $objProperty->getMinWeeks()) {
                $blnToReturn = false;
                $errors[] = $objProperty->getMinWeeks()." nights minimum";  
            }
        }

        if (is_null($customerId)) {
            $blnToReturn = false;
            $errors[] = "Please choose a customer in the list";
        }

        if ($nbAdults != "" && !is_numeric($nbAdults)) {
            $blnToReturn = false;
            $errors[] = "Invalid number for Nb.Adults";    
        }
        else {
            if ($nbChildren != "" && !is_numeric($nbChildren)) {
                $blnToReturn = false;
                $errors[] = "Invalid number for Nb.Children"; 
            }
            else {
                $maxSleeps = (!is_null($objProperty->getMaxSleeps())) ? $objProperty->getMaxSleeps() : 0 ;
                if (($nbAdults + $nbChildren) > $maxSleeps) {
                    $blnToReturn = false;
                    $errors[] = "Maximum of sleeps for this property is ".$maxSleeps;  
                }
            }
        }

        return $errors;
        
    }
    
}