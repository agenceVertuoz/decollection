<?php

namespace App\Vertuoz\Api\Helper;

class ContentHelper {

    protected $client;

    public function __construct(\App\Vertuoz\Api\ApiClient $client) {
        $this->client = $client;
    }

    public static function slugify($str, $delimiter = '-'){
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;
    }

    public function listContents($params) {
        return $this->client->get('/contents?' . http_build_query($params));
    }

    public function get($id) {
        
        $content = $this->client->get('/contents?id=' . $id);
        
        if (!empty($content)) {
            return $content[0];
        }
        return null;
        
    }

    public function getOneByCode($code) {
        
        $contents = $this->client->get('/contents?code=' . $code);

        if (!empty($contents)) {
            return $contents[0];
        }
        return null;
    }
    
    public function getOneByReadMoreLink($url) {

        $contents = $this->client->get('/contents?readMoreLink=/' . $url);
        if (!is_null($contents)) {
            return $contents[0];
        }
        return null;
    }

    public function getOneByReadMoreLinkAndAreaNameNotIn($url, $excludes)
    {
        $contents = $this->client->get('/contents?readMoreLink=/' . $url);
        foreach ($contents as $content) {
            if (!in_array($content['areaName'], $excludes)) {
                return $content;
            }
        }
        return null;
    }

    public function getAllByArea($code) {
        $contents = $this->client->get('/contents?areaName=' . $code);

        return $contents;
    }

}
