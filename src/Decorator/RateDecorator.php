<?php

namespace App\Decorator;

use App\Vertuoz\Decorator\AbstractDecorator;

class RateDecorator extends AbstractDecorator {

    const COMPONENT_CLASS = '\ModSpecific30Price';
    
    public $startDate;
    public $endDate;
    public $minimumNightStay;
    public $nightlyRate;
    
    public function __construct($component) {

        parent::__construct($component);
        
        $this->startDate = $this->getStartDate();
        $this->endDate = $this->getEndDate();
        $this->minimumNightStay = $this->getMinimumNightStay();
        $this->nightlyRate = $this->getNightlyRate(7);
                
    }
    
    public function getStartDate() {
        return $this->StartDate;
    }
    
    public function getEndDate() {
        return $this->EndDate;
    }
    
    public function getMinimumNightStay() {
        return $this->MinimumNightStay;
    }

}
