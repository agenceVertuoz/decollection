<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use App\Helper\TransactionFormHelper;
use App\Helper\TransactionHelper;

class BaseTransactionController extends Controller {

    protected $objProperty;
    protected $objPartner;
    protected $objCustomer;
    protected $type;
    protected $arrivalDate;
    protected $departureDate;
    protected $nbAdults;
    protected $nbChildren;
    protected $status;
    protected $netRental;
    protected $netOwner;
    protected $netExternal;
    protected $commission;
    protected $costRental;
    protected $matchedPrices;
    protected $twigData = [];

    public function init() {

        $request = Request::createFromGlobals();

        $errors = [];
        $twigData = [];

        $transaction = $request->query->get('transaction');

        $propertyId = $transaction['property_id'];
        $customerId = $transaction['customer'];

        $this->type = $transaction['type'];
        $this->arrivalDate = $transaction['arrival_date'];
        $this->departureDate = $transaction['departure_date'];
        $this->nbAdults = $transaction['nb_adults'];
        $this->nbChildren = $transaction['nb_children'];

        $this->objProperty = $this->get('property.service')->loadById($propertyId);
        if (!$this->objProperty) {
            throw new NotFoundHttpException('The property does not exist');
        }

        $this->objPartner = $this->get('partner.service')->loadByUsername($this->get('security.token_storage')->getToken()->getUser()->getUsername());
        if (!$this->objPartner) {
            throw new NotFoundHttpException('The partner does not exist');
        }

        $this->objCustomer = $this->get('customer.service')->loadById($customerId);
        if (!$this->objCustomer) {
            throw new NotFoundHttpException('The customer does not exist');
        }

        $errors = TransactionFormHelper::getErrors($this->objProperty, $customerId, $this->arrivalDate, $this->departureDate, $this->nbAdults, $this->nbChildren);
        
        
        if (count($errors) == 0) {
            
            $arrAvailabilities = $this->get('availability.service')->loadAllByPropertyId($this->objProperty->Id);
            $arrTransactions = $this->get('transaction.service')->loadAllByPropertyId($this->objProperty->Id);
        
            $isValid = TransactionHelper::isValidTransaction($arrAvailabilities, $arrTransactions, $this->arrivalDate, $this->departureDate, $this->type);

            if ($isValid) {
                
                $this->netRental = TransactionHelper::getNetRental($this->objProperty, $this->arrivalDate, $this->departureDate);
                $this->commission = TransactionHelper::getCommission($this->netRental, $this->objPartner->getPercentCommission());
                $this->netOwner = TransactionHelper::getNetOwner($this->objProperty, $this->arrivalDate, $this->departureDate);
                $this->netExternal = TransactionHelper::getNetExternal($this->objProperty, $this->arrivalDate, $this->departureDate);
                $this->costRental = TransactionHelper::getCostRental($this->netRental, $this->commission);
                $this->matchedPrices = TransactionHelper::getMatchedPrices($this->objProperty, $this->arrivalDate, $this->departureDate);

                $this->twigData['property'] = $this->objProperty;
                $this->twigData['partner'] = $this->objPartner;
                $this->twigData['customer'] = $this->objCustomer;
                $this->twigData['arrivalDate'] = $this->arrivalDate;
                $this->twigData['departureDate'] = $this->departureDate;
                $this->twigData['nbAdults'] = $this->nbAdults;
                $this->twigData['nbChildren'] = $this->nbChildren;
                $this->twigData['netRental'] = $this->netRental;
                $this->twigData['netOwner'] = $this->netOwner;
                $this->twigData['netExternal'] = $this->netExternal;
                $this->twigData['matchedPrices'] = $this->matchedPrices;
                $this->twigData['commission'] = $this->commission;
                $this->twigData['costRental'] = $this->netRental - $this->commission;
            
            }
            else {
                $errors[] = "Unable to create a new reservation on this property for this period.";
            }
            
        }

        $this->twigData['errors'] = $errors;
    }


}
