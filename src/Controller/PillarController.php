<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PillarController extends Controller {

    /**
     * @Route("/pillar/show", name="pillar_show")
     */
    public function show(Request $request) {

        $twigData = [];

        $pillarId = $request->query->get('pillarId');
        $propertyId = $request->query->get('propertyId');
        
        $objJoinPillarProperty = $this->get('service.pillar')->loadPillarByPropertyId($pillarId, $propertyId);
        if (!$objJoinPillarProperty) {
            throw new NotFoundHttpException('The Pillar does not exist');
        }
        
        //TODO - Twig (tom)
        return $this->render('pillar/show.html.twig', [
            'name' => $objJoinPillarProperty->getName(),
            'description' => $objJoinPillarProperty->getDescription(),
            'image' => $objJoinPillarProperty->getImage()
        ]);
        
    }

}



