<?php

namespace App\Decorator;

use App\Vertuoz\Decorator\AbstractDecorator;

class AvailabilityDecorator extends AbstractDecorator {

    const COMPONENT_CLASS = '\ModSpecific30Availability';

    
    public function __construct($component) {

        parent::__construct($component);
        
                
    }
    

}
