<?php

namespace App\Helper;
use App\Helper\UtilsHelper;
use App\Service\PartnerService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PartnerFormHelper {
    
    const PASSWORD_MIN_LENGTH = 6;
    
    protected $container;
    
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }
    
    public static function getCountriesChoices($arrCountries) {
        
        $countries = [];
        
        if (!is_null($arrCountries)) {
            foreach ($arrCountries as $objCountry) {
                $countries[$objCountry->Name] = $objCountry->Id;
            }
        }
        
        return $countries;
        
    }
    
    public function getFormPartnerErrors($data) {
        
        $errors = [];
        $blnToReturn = true;
        
        // Vérification de la validité de l'e-mail
        if (!UtilsHelper::isValidEmail($data['email'])) {
            $blnToReturn = false;
            $errors[] = "Please enter a valid e-mail address";
        }
        
        // Vérification de l'unicité de l'e-mail
        $objCheckPartner = PartnerService::loadByEmail($data['email']);
        if (is_array($objCheckPartner) && count($objCheckPartner) > 1) {
            $blnToReturn = false;
            $errors[] = "E-mail address already in use";
        }
        
        return $errors;
        
    }
    
    
    public function getFormPartnerPasswordErrors($objPartner, $objUser, $data) {
        
        $errors = [];
        $blnToReturn = true;
        
        $passwordEncoder = $this->container->get("security.password_encoder");
        
        if (isset($data['current_password'])) {
            
            $currentEncodedPassword = $passwordEncoder->encodePassword($objUser, $data['current_password']);

            if ($objPartner->getPassword() != $currentEncodedPassword) {
                $blnToReturn = false;
                $errors[] = "Current password is invalid";
            }
        
        }
        
        if ($data['new_password'] != $data['new_password_confirm']) {
            $blnToReturn = false;
            $errors[] = "New password and new password confirmation must match";
        }
        
        if (strlen($data['new_password']) < self::PASSWORD_MIN_LENGTH) {
            $blnToReturn = false;
            $errors[] = "New password must be ". self::PASSWORD_MIN_LENGTH ." characters min.";
        }
        
        return $errors;
        
    }
    
}