<?php

namespace App\Vertuoz\Api\Service;

use App\Vertuoz\Api\Model\Slider AS SliderModel;
use App\Vertuoz\Api\Service\SiteService AS SiteService;

class SliderService {

    protected $db;
    protected $appId;
    protected $slideshowModel = null;
    protected $siteModel = null;

    public function __construct($db, $appId) {

        $this->db = $db;
        $this->appId = $appId;
        $this->slideshowModel = new SliderModel($this->db, $this->appId);

        $siteService        = new SiteService($this->db, $this->appId);
        $this->siteModel    = $siteService->get();
    }

    public function getOne($params)
    {
        if(!isset($params['language']))
            $params['language'] = $this->siteModel->getLanguage();
        else {
            if(!in_array(strtoupper($params['language']), $this->siteModel->getLanguagesAvailables())) {
                throw new \Exception("Langue '".$params['language']."' non autorisée", 400);
            }
            else
                $this->siteModel->setLanguage($params['language']);
        }

        return $this->slideshowModel->getOne($params);
    }

    public function getAllByFieldsValue($params) {
        $allowedFields = array("areaName", "siteId", "name", "language");

        foreach ($params as $k => $v) {
            if (!in_array($k, $allowedFields)) {
                throw new \Exception("Filtre $k non autorisé, filtres attendus " . implode(", ", $allowedFields), 400);
            }
        }

        if(!isset($params['language']))
            $params['language'] = $this->siteModel->getLanguage();
        else {
            if(!in_array(strtoupper($params['language']), $this->siteModel->getLanguagesAvailables())) {
                throw new \Exception("Langue '".$params['language']."' non autorisée", 400);
            }
            else
                $this->siteModel->setLanguage($params['language']);
        }

        $results = $this->slideshowModel->getAll($params);

        return $results;
    }

}
