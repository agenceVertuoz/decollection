<?php

namespace App\Service;
use App\Decorator\AvailabilityDecorator;

class AvailabilityService {

    public function loadAllByPropertyId($propertyId) {
        
        $arrAvailabilities = \ModSpecific30Availability::QueryArray(
            \QQ::AndCondition(
                    \QQ::Equal(\QQN::ModSpecific30Availability()->PropertyIdObject->Id, $propertyId)
            ),
            \QQ::Clause(
                 \QQ::OrderBy(
                       \QQN::ModSpecific30Availability()->StartDate, true  
                 )
            )
        );
        
        $arrDecoratedAvailabilities = self::decorateCollection($arrAvailabilities);

        return $arrDecoratedAvailabilities;
        
    }
    
    public static function decorateCollection(array $arrAvailabilities) {

        $arrDecoratedAvailabilities = [];
        
        foreach ($arrAvailabilities as $objAvailability) {
            $objNewAvailability = self::decorate($objAvailability);
            $arrDecoratedAvailabilities[] = $objNewAvailability;
        }
        
        return $arrDecoratedAvailabilities;
        
    }
    
    public static function decorate($objAvailability) {

        $objDecoratedAvailability = new AvailabilityDecorator($objAvailability);

        return $objDecoratedAvailability;
        
    }

}
