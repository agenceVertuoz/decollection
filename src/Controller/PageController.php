<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Helper\TransactionHelper;

class PageController extends Controller {

    /**
     * @Route("/rental-conditions", name="rental_conditions")
     */
    public function rentalConditions()
    {
        
        $rentalConditions = file_get_contents(TransactionHelper::RENTAL_CONDITIONS_PATH);
        
        return $this->render('print.html.twig', [
            'content' => $rentalConditions,
            'page' => 'app',
            'current_nav' => ''
        ]);
    }
    
}