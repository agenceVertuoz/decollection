<?php

namespace App\Service;


//require_once('../../luxuryElocms/includes/sites/administration/class.smtp.php');


use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class MailerService {
    
    protected $params;
    protected $phpMailer;

    public function __construct(ParameterBagInterface $params, \PHPMailer $phpMailer) {
        
        $this->params = $params;
        
        $this->phpMailer = $phpMailer;
//        $this->phpMailer->SMTPDebug = true;
        $this->phpMailer->IsSMTP();
        $this->phpMailer->isHTML(true); 
        $this->phpMailer->SingleTo = false;
        $this->phpMailer->CharSet = "UTF-8";
        $this->phpMailer->SMTPAuth = true;
        $this->phpMailer->Host = $this->params->get('mailer.host');
        $this->phpMailer->Hostname = $this->params->get('mailer.hostname');
        $this->phpMailer->Helo = $this->params->get('mailer.helo');
        $this->phpMailer->From = $this->params->get('mailer.from');
        $this->phpMailer->FromName = $this->params->get('mailer.fromName');
        $this->phpMailer->Username = $this->params->get('mailer.username');
        $this->phpMailer->Password = $this->params->get('mailer.password');

    }
    
    public function sendForgotPassword($objPartner, $url) {
        
        $subject = "Luxury Rentals Agents - Your password request";
        $mailHtml = "<p>Hello ".$objPartner.",</p>";
        $mailHtml .= "<p>It seems you forgot your password. Please, <a href=\"".$url."\">click on this link to set a new password.</a></p>";
        $mailHtml .= "<br><br>Best regards,<br><br>Luxrury Rentals Agents";
   
        $this->phpMailer->ClearAddresses();
        $this->phpMailer->AddAddress($objPartner->getEmail());
        $this->phpMailer->Subject = $subject;
        $this->phpMailer->MsgHtml($mailHtml);

        return $this->phpMailer->Send();
        
    }
    
    public function getMailer() {
        return $this->phpMailer;
    }

}
