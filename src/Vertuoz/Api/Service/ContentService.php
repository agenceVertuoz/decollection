<?php

namespace App\Vertuoz\Api\Service;

use App\Vertuoz\Api\Model\Content AS ContentModel;
use App\Vertuoz\Api\Service\SiteService AS SiteService;

class ContentService {

    protected $db;
    protected $appId;
    protected $contentModel = null;
    protected $siteModel = null;

    public function __construct($db, $appId) {

        $this->db           = $db;
        $this->appId        = $appId;
        $this->contentModel = new ContentModel($this->db, $this->appId);

        $siteService        = new SiteService($this->db, $this->appId);
        $this->siteModel    = $siteService->get();
    }

    public function getOne($params)
    {
        $params = $this->checkFilters($params);

        return $this->contentModel->getOne($params);
    }

    public function getAllByFieldsValue($params) {

        $params = $this->checkFilters($params);

        $results = $this->contentModel->getAll($params);

        return $results;
    }

    private function checkFilters($params) {

        //Vérififcation champs
        $allowedFields     = array("keyword","id","language","areaName", "siteId", "code", "readMoreLink", "extrafield", "start", "limit", "order" , "direction");

        foreach ($params as $k => $v) {

            if (!in_array($k, $allowedFields)) {
                throw new \Exception("Filtre $k non autorisé, filtres attendus : " . implode(", ", $allowedFields), 400);
            }
        }
        
        
        if (isset($params['id'])) {
            $params['content.id'] = $params['id'];
            unset($params['id']);
        }
        
        if (isset($params['siteId'])) {
            $params['content.siteId'] = $params['siteId'];
            unset($params['siteId']);
        }

        //Définition de la langue par défaut si non précisé, vérification si ok dans le site si précisée
        if(!isset($params['language']))
            $params['language'] = $this->siteModel->getLanguage();
        else {
            if(!in_array(strtoupper($params['language']), $this->siteModel->getLanguagesAvailables())) {
                throw new \Exception("Langue '".$params['language']."' non autorisée", 400);
            }
            else
                $this->siteModel->setLanguage($params['language']);
        }

        return $params;
    }
}
