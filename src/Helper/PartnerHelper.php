<?php

namespace App\Helper;

class PartnerHelper {
    
    public static function generateKey($objPartner, $appKey) {
        return md5($objPartner->getId().$appKey);
    }
    
    public static function isValidKey($objPartner, $key, $appKey) {
        return md5($objPartner->getId().$appKey) == $key;
    }
    
}