<?php

namespace App\Vertuoz\Api\Model;

class Page {
    
    protected $siteId;
    
    public function __construct($siteId) {
        $this->siteId = $siteId;
    }

    public function loadAll() {

        return \Page::QueryArray(
            \QQ::Equal(\QQN::Page()->SiteId, $this->siteId), 
            \QQ::Clause(\QQ::OrderBy(\QQN::Page()->Url, false))
        );
        
    }

}
