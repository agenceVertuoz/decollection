<?php

namespace App\Service;
use App\Decorator\PillarDecorator;

class PillarService {

    public function loadPillarByPropertyId($pillarId, $propertyId) {
        
        $objPillar = \ModSpecific30JoinPillarProperty::QuerySingle(
            \QQ::AndCondition(
                \QQ::Equal(\QQN::ModSpecific30JoinPillarProperty()->PropertyId, $propertyId),
                \QQ::Equal(\QQN::ModSpecific30JoinPillarProperty()->PillarId, $pillarId)
            )
        );
        
        $objDecoratedPillar = self::decorate($objPillar);

        return $objDecoratedPillar;
        
    }
    
    
    public static function decorate($objPillar) {

        $objDecoratedPillar = new PillarDecorator($objPillar);

        return $objDecoratedPillar;
        
    }

}
