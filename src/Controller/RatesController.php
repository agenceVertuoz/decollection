<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Service\RateService;
use App\Service\PropertyService;

class RatesController extends Controller
{
    /**
     * @Route("/rates/list", name="rates_list")
     */
    public function index(Request $request)
    {
        
        $arrPrices = [];
        
        if ($request->isXmlHttpRequest()) {
            
            $propertyId = $request->query->get('property_id');
            
            $objProperty = $this->get('property.service')->loadById($propertyId);
            $arrPrices = $this->get('rate.service')->loadAllByPropertyId($propertyId);
      
        }
        
        return $this->render('rates/list.html.twig', [
            'property' => $objProperty,
            'prices' => $arrPrices
        ]);
    }
}
