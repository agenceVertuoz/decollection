
module.exports = {
    getRatesByPropertyId: getRatesByPropertyId
};

var $modal = $('#default-modal');

function getRatesByPropertyId(propertyId) {
    var jxhr = $.get('rates/list?property_id=' + propertyId, function (data) {
        $modal.find('.modal-title').text("Rates");
        $modal.find('.modal-body').html(data);
        $modal.modal('show');
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
}


